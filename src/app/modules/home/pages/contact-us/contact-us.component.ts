import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { CommonService } from '../../../../core/services/common.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  contactForm = this.fb.group({
    name: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    cel: ['', Validators.required],
    message: ['', Validators.required]
  })

  constructor(
    private fb: FormBuilder,
    private commonService: CommonService
  ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    if (this.contactForm.invalid) return Swal.fire('', 'Preencha todos os campos corretamente antes de continuar', 'warning');

    this.commonService.sendMail(this.contactForm.value).subscribe(
      response => {
        Swal.fire('', 'Email enviado com sucesso.', 'success')    
      },
      error => {
        Swal.fire('', 'Houve um erro ao enviar seu email. Tente novamente mais tarde !.', 'error')  
      }
    )
  }

}
