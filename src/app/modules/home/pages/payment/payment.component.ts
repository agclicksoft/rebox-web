
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import Swal from 'sweetalert2';
import { PaymentService } from '../../../../core/services/payment.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { RequestTownService } from '../../../../core/services/request-town.service';
import { JsonPipe } from '@angular/common';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  isTowByPlan = false;

  payment_method: string;
  validity: string;

  paymentForm = this.fb.group({
    cpf: ['', Validators.required],
    cred_name: ['', Validators.required],
    cred_number: ['', Validators.required],
    cred_due_date_month: [''],
    cred_due_date_year: [''],
    cred_verification: ['', Validators.required],
    portion: [1],
    validity: ['', Validators.required],
    payment_mode: [],
    type_payment_mode: []
  });
  reboxService: any;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private paymentService: PaymentService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private requestTowComponentService: RequestTownService
  ) { }

  ngOnInit(): void {
    this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.requestTowComponentService.getOneReboxService(params['rebox']).then(
          data => {
            this.reboxService = data[0];
            if (data[0].users_plan_id && data[0].amount === 0) {
              this.isTowByPlan = true;
            }
            console.log(data);
          }
        ).catch(
          error => console.log(error)
        );
      });
  }

  cashType() {
    this.spinner.show();
    this.paymentForm.controls.payment_mode.patchValue('dinheiro');
    this.paymentService.requestPayment(this.reboxService.id, this.paymentForm.value).subscribe(
      response => {
        this.router.navigate(['/tow-tracking'],  { queryParams: { rebox: this.reboxService.id } });
        this.spinner.hide();
      },
      error => {
        Swal.fire('Oops...', 'Houve um erro ao processar seu pagamento. Tente novamente mais tarde', 'error');
        this.spinner.hide();
      }
    )
  }

  changeValue() {
    const is_credit_card = document.getElementById('is_credit_card') as HTMLInputElement;
    const is_debit_card = document.getElementById('is_debit_card') as HTMLInputElement;
    const is_cash = document.getElementById('is_cash') as HTMLInputElement;

    for (const control in this.paymentForm.controls) {
      this.paymentForm.get(control).clearValidators();
    }

    this.paymentForm.patchValue({
      cpf: null,
      cred_due_date_month: null,
      cred_due_date_year: null,
      cred_name: null,
      cred_number: null,
      cred_verification: null,
      type_payment_mode: null,
      validity: null,
    });

    if(is_credit_card.checked) {
      this.paymentForm.get('type_payment_mode').patchValue('Crédito');
    }
    if(is_debit_card.checked) {
      this.paymentForm.get('type_payment_mode').patchValue('Débito');
    }
    if (is_cash.checked) {
      this.paymentForm.get('type_payment_mode').patchValue('Dinheiro');
    }
  }

  onSubmitByPlanFree() {
    for (const control in this.paymentForm.controls) {
      this.paymentForm.get(control).clearValidators();
    }
    this.paymentForm.get('type_payment_mode').patchValue('plano');
    this.paymentForm.get('payment_mode').patchValue('presencial');

    this.spinner.show();
    this.paymentService.requestPayment(this.reboxService.id, this.paymentForm.value).subscribe(
      response => {
        this.router.navigate(['/tow-tracking'],  { queryParams: { rebox: this.reboxService.id } });
        this.spinner.hide();
      },
      error => {
        Swal.fire('Oops...', 'Houve um erro ao processar seu pagamento. Tente novamente mais tarde', 'error');
        this.spinner.hide();
      }
    );
  }

  onSubmit() {
    if (this.paymentForm.invalid) {
      return Swal.fire('', 'Preencha todos os campos corretamente antes de continuar', 'warning');
    }

    this.spinner.show();

    this.paymentForm.controls.payment_mode.patchValue(this.payment_method);

    if(this.paymentForm.get('payment_mode').value === 'presencial') {
      this.paymentForm.get('type_payment_mode').setValidators(Validators.required);
    }

    if(this.paymentForm.get('validity').value) {
      this.paymentForm.controls.cred_due_date_month.patchValue(this.paymentForm.value.validity.substr(0, 2));
      this.paymentForm.controls.cred_due_date_year.patchValue(this.paymentForm.value.validity.substr(2));
    }

    this.paymentService.requestPayment(this.reboxService.id, this.paymentForm.value).subscribe(
      response => {
        this.router.navigate(['/tow-tracking'],  { queryParams: { rebox: this.reboxService.id } });
        this.spinner.hide();
      },
      error => {
        Swal.fire('Oops...', 'Houve um erro ao processar seu pagamento. Tente novamente mais tarde', 'error');
        this.spinner.hide();
      }
    );
    sessionStorage.removeItem('signup');
    sessionStorage.removeItem('additionalInfo');
    sessionStorage.removeItem('location');
    // TODO: PAYMENT REQUEST
    // this.router.navigate(['/'])
  }

}
