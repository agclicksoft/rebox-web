import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseACarComponent } from './choose-a-car.component';

describe('ChooseACarComponent', () => {
  let component: ChooseACarComponent;
  let fixture: ComponentFixture<ChooseACarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseACarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseACarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
