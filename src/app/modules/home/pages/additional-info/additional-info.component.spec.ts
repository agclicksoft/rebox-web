import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdditionalInfoComponent } from './additional-info.component';

describe('TowingFormComponent', () => {
  let component: TowingFormComponent;
  let fixture: ComponentFixture<TowingFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdditionalInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TowingFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
