import { ActivatedRoute } from '@angular/router';
import { UtilsService } from './../../../../core/services/utils.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { RequestTownService } from '../../../../core/services/request-town.service';

import Swal from 'sweetalert2';
import * as moment from 'moment';

@Component({
  selector: 'app-additional-info',
  templateUrl: './additional-info.component.html',
  styleUrls: ['./additional-info.component.css']
})
export class AdditionalInfoComponent implements OnInit {

  diagnostic: string;
  diagnosticDescription: string;
  location: string;
  locationDescription: string;
  observations: string;

  isSubmitted = false;

  dataLocation = null;

  diagnostics: Array<any> = [
    { id: 1, description: 'Parou de funcionar'},
    { id: 2, description: 'Problema na roda'},
    { id: 3, description: 'Câmbio travado'},
    { id: 4, description: 'Capotado'},
    { id: 5, description: 'Roda furtada'},
    { id: 6, description: 'Nenhuma das opções'}
  ];

  locations: Array<any> = [
    { id: 1, description: 'Via pública'},
    { id: 2, description: 'Ribanceira/Fora da via'},
    { id: 3, description: 'Garagem subsolo'},
    { id: 4, description: 'Garagem nível da rua'}
  ];

  constructor(
    private requestTownService: RequestTownService,
    private UtilsService: UtilsService,
    private route: ActivatedRoute
  ) {
    this.route.queryParams.subscribe(response => {
      console.log(response)
      if (response) {
        const { lat1, lat2, lng1, lng2, use_plan } = response;
        this.dataLocation = {
          lat1,
          lat2,
          lng1,
          lng2,
          use_plan: use_plan === 'true' ? true : false
        };
      }
    })
  }

  ngOnInit(): void {
    if (sessionStorage.getItem('additionalInfo')) {
      this.getDataStore();
    }
  }

  getDataStore() {
   const info = JSON.parse(sessionStorage.getItem('additionalInfo'));
   this.diagnostic = info.status_vehicle_id;
   this.location = info.removal_location_id;
   this.observations = info.comments;
   this.diagnosticDescription = info.diagnosticDescription;
   this.locationDescription = info.locationDescription;
  }

  saveOnStorage() {
    sessionStorage.setItem('additionalInfo', JSON.stringify({
      status_vehicle_id: this.diagnostic,
      removal_location_id: this.location,
      comments: this.observations,
      diagnosticDescription: this.diagnosticDescription,
      locationDescription: this.locationDescription,
      expiration_at: moment().add(5, 'minutes').valueOf()
    }));
  }

  onSubmit() {
    this.isSubmitted = true;
    if (!this.diagnostic || !this.location) {
       return Swal.fire('', 'Preencha todos os campos corretamente antes de continuar', 'warning');
    }

    const infoForm = {
      status_vehicle_id: this.diagnostic,
      removal_location_id: this.location,
      comments: this.observations
    }

    if (this.dataLocation.lat1) {
      console.log(this.dataLocation)
      const { lat1, lat2, lng1, lng2, use_plan } = this.dataLocation;
      const data = {
        status_vehicle_id: this.diagnostic,
        removal_location_id: this.location,
        comments: this.observations,
        lat1: parseFloat(lat1),
        lat2: parseFloat(lat2),
        lng1: parseFloat(lng1),
        lng2: parseFloat(lng2),
        use_plan
      }
      return this.requestTownService.setLocation(data);
    }

    this.requestTownService.onAdditionalInfoSubmit(infoForm);
    this.saveOnStorage();
  }

}
