import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import * as Ws from '@adonisjs/websocket-client';
import Swal from 'sweetalert2';
import { RequestTownService } from '../../../../core/services/request-town.service';
import { environment } from '../../../../../environments/environment';

declare var $: any;

@Component({
  selector: 'app-track-town',
  templateUrl: './track-town.component.html',
  styleUrls: ['./track-town.component.css']
})
export class TrackTownComponent implements OnInit {
  isFinished: boolean = false;
  arrived: boolean = false;
  reboxService: any;
  private socket;
  private channel;

  comments: string;
  value: any;
  constructor(
    private route: ActivatedRoute,
    private requestTowComponentService: RequestTownService
  ) { }

  ngOnInit(): void {
    this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.requestTowComponentService.getOneReboxService(params['rebox']).then(
          data => {
            this.reboxService = data[0];
            console.log(this.reboxService);
          }
        ).catch(
          error => console.log(error)
        );
      });

    //TODO - PERSONALIZAR CSS
    Swal.fire({
      title: 'Sua solicitação foi realizada com sucesso',
      text: '',
      confirmButtonText: 'Acompanhar'
    })

    this.subscribeInChannel();
  }

  async toEvaluate() {
    if ($("#ratingForm :radio:checked").length == 0) {
      $('#status').html(' Avalie para finalizar ! ');
      return false;
    }
    else {
      this.value = $('input:radio[name=rating]:checked').val();
      this.comments = $('#comments').val();
      //TODO - NAVIGATE TO MY PROFILE
      await this.requestTowComponentService.toEvaluate(this.value, this.comments, this.reboxService.id).then(
        resolve => {
          $('#rating').modal('hide');
        },
        reject => {
          console.log(reject)
        }
      )
    }
  }

  subscribeInChannel() {
    this.socket = Ws(environment.SOCKET_ENDPOINT, { debug: true });

    this.socket.connect();

    // conectando-se ao canal
    this.channel = this.socket.subscribe('notifications');

    // escutando as atualizações do canal quando o método 'call' foi invocado
    this.channel.on('change:rebox', event => {
      if (event.status == 'andamento' && event.id == this.reboxService.id) {
        this.reboxService = event
        Swal.fire({
          title: 'Seu reboque está a caminho.',
          text: '',
          confirmButtonText: 'Acompanhar'
        })
        this.subscribeInChannel();
      }

      if (event.status == 'finalizado' && event.id == this.reboxService.id) {
        $('#rating').modal('show');
      }
    });
  }

  confirmPresent(reboxID) {
    this.requestTowComponentService.modifyRebox({is_present: 1}, reboxID);
  }


}
