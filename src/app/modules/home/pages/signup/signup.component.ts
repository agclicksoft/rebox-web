import { UtilsService } from './../../../../core/services/utils.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { RequestTownService } from '../../../../core/services/request-town.service';

import Swal from 'sweetalert2'
import { UserService } from '../../../../core/services/user.service';
import { RequestService } from '../../../../core/services/request.service';
import { CarService } from '../../../../core/services/car.service';

import * as moment from 'moment';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  isSubmited: boolean = false;
  userExists: boolean = false;
  carBrands: Array<any> = [];
  carModels: Array<any> = [];
  signUpForm = this.fb.group({
    name: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    port_vehicle: [''],
    // password: ['', [Validators.required, Validators.minLength(6)]],
    cel: ['', Validators.required],
    vehicle: this.fb.group({
      brand: [null, Validators.required],
      model: [null, Validators.required],
      board: ['', Validators.required],
      is_armored: ['', Validators.required]
    })
  });


  constructor(
    private fb: FormBuilder,
    private requestTownService: RequestTownService,
    private requestService: RequestService,
    private userService: UserService,
    private carService: CarService,
    private utilsService: UtilsService
  ) { }

  ngOnInit(): void {
    this.getCarBrands({term: ''});
    if(sessionStorage.getItem('signup')) {
      this.getSignupData();
    }
  }

  getSignupData() {
    this.signUpForm.patchValue(JSON.parse(sessionStorage.getItem('signup')));
  }

  get vehicle() {
    return this.signUpForm.controls.vehicle as FormGroup;
  }

  checkIfExists(email) {
    // const data = {email};
    // this.userService.checkIfExists(data).subscribe(
    //   response => {
    //     this.userExists = true;
    //   },
    //   error => {
    //     if(error.status == 404) this.userExists = false;
    //   }
    // )
  }

  getCarModels(event) {
    const model = event.term || '';
    const brand = this.vehicle.value.brand;
    this.carService.getModels(brand, model).subscribe(
      response => {
        this.carModels = response;
        this.carModels.push(
          {
            model: 'MOTO',
            port_car_id: 8
          },
          {
            model: 'TRICÍCLO',
            port_car_id: 2
          },
          {
            model: 'QUADRICÍCLO',
            port_car_id: 3
          }
        );
      },
      error => {
        Swal.fire('Oops...', 'Houve um erro ao buscar os modelos de veículos', 'error')
      }
    )
  }

  getCarBrands(event) {
    const brand = event.term;
    this.carService.getBrands(brand).subscribe(
      response => {
        this.carBrands = response;
      },
      error => {
        Swal.fire('Oops...', 'Houve um erro ao buscar as marcas de veículos', 'error')
      }
    );
  }

  setPortVehicle(event) {
    this.signUpForm.get('port_vehicle').patchValue(event.port_car_id);
  }

  saveOnStorage() {
    const signup = {
      name: this.signUpForm.get('name').value,
      email: this.signUpForm.get('email').value,
      cel: this.signUpForm.get('cel').value,
      vehicle: {
        brand: this.signUpForm.get('vehicle').get('brand').value,
        model: this.signUpForm.get('vehicle').get('model').value,
        board: this.signUpForm.get('vehicle').get('board').value,
        is_armored: this.signUpForm.get('vehicle').get('is_armored').value,
      }
    };
    sessionStorage.setItem('signup', JSON.stringify(this.signUpForm.value));
  }

  onSubmit() {

    this.isSubmited = true;

    if (this.signUpForm.invalid) { return Swal.fire('', 'Preencha todos os campos corretamente antes de continuar', 'warning'); }

    this.requestTownService.onSignUpSubmit(this.signUpForm.value);
    this.saveOnStorage();


  //   if (this.userExists) {
  //     const data = {email: this.signUpForm.value.email, password: this.signUpForm.value.password};

  //     this.userService.authenticate(data).subscribe(
  //       response => {
  //         this.requestTownService.onSignUpSubmit(this.signUpForm.value)
  //       },
  //       error => {
  //         Swal.fire('Oops...', 'Email e/ou senha inválidos.', 'error');
  //       }
  //     )
  //   }
  //   else {
  //     this.requestTownService.onSignUpSubmit(this.signUpForm.value)
  //   }
  }
}
