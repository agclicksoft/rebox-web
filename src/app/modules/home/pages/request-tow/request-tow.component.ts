import { UtilsService } from './../../../../core/services/utils.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { RequestTownService } from '../../../../core/services/request-town.service';

import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import { PlaceService } from '../../../../core/services/place.service';
import { CommonService } from '../../../../core/services/common.service';
import * as moment from 'moment';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
declare var $ :any;

@Component({
  selector: 'app-request-tow',
  templateUrl: './request-tow.component.html',
  styleUrls: ['./request-tow.component.css']
})
export class RequestTowComponent implements OnInit {

  originList: Array<any> = [];
  origin: any;
  destiny: any;

  addressOrigin;
  addressDestiny;
  fullAddressOrigin;
  fullAddressDestiny;
  requestForm = this.fb.group({
    lat1: ['', Validators.required],
    lng1: ['', Validators.required],
    lat2: ['', Validators.required],
    lng2: ['', Validators.required]
  });

  contactForm = this.fb.group({
    name: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    cel: ['', Validators.required],
    message: ['', Validators.required]
  });

  options = {
    componentRestrictions: { country: 'BR' }
  };

  @ViewChild('placesRef') placesRef: any;

  constructor(
    private fb: FormBuilder,
    private requestTownService: RequestTownService,
    private placeService: PlaceService,
    private spinner: NgxSpinnerService,
    private commonService: CommonService,
    private utilsService: UtilsService
  ) { }

  ngOnInit(): void {
    if (sessionStorage.getItem('location')) {
      this.getLocationStorage();
    }

  }

  // test() {
  //   const origin = document.getElementById('origin') as HTMLInputElement;
  //   const num = document.getElementById('origin_number') as HTMLInputElement;
  //   const autcomplete = new google.maps.places.Autocomplete(this.placesRef.nativeElement, {
  //     types: ['address']
  //   });
  //   console.log(autcomplete);
  //   autcomplete.addListener('place_changed', () => {
  //     const place = autcomplete.getPlace();
  //     place.address_components.unshift({long_name: num.value, short_name: num.value, types: ['street_number']});
  //     console.log(place);
  //     console.log(place.geometry.location.lat());
  //     console.log(place.geometry.location.lng());
  //   });
  // }

  // SELECTED ADDRESS ORIGIN BY PLACE API
  setOrigin(address: Address) {
    this.addressOrigin = `${address.name} - ${address.formatted_address}`;
    this.fullAddressOrigin = `${address.name} - ${address.formatted_address}`;
    document.getElementById('originGps').classList.replace('d-block', 'd-none');
    this.requestForm.controls.lat1.patchValue(address.geometry.location.lat().toFixed(10));
    this.requestForm.controls.lng1.patchValue(address.geometry.location.lng().toFixed(11));
  }

  // SELECTED ADDRESS DESTINY BY PLACE API
  setDestiny(address: any) {
    this.addressDestiny = `${address.name} - ${address.formatted_address}`;
    this.fullAddressDestiny = `${address.name} - ${address.formatted_address}`;
    document.getElementById('destinyGps').classList.replace('d-block', 'd-none');
    this.requestForm.controls.lat2.patchValue(address.geometry.location.lat().toFixed(10));
    this.requestForm.controls.lng2.patchValue(address.geometry.location.lng().toFixed(11));
  }

  // SHOW / HIDE MY ORIGIN LOCATION OPTION
  originFocus(event) {
    event.length > 0 ? document.getElementById('originGps').classList.replace('d-none', 'd-block')
      : document.getElementById('originGps').classList.replace('d-block', 'd-none');
  }

  // SHOW / HIDE MY DESTINY LOCATION OPTION
  destinyFocus(event) {
    event.length > 0 ? document.getElementById('destinyGps').classList.replace('d-none', 'd-block')
    : document.getElementById('destinyGps').classList.replace('d-block', 'd-none');
  }

  // GET MY ORIGIN LOCATION
  getOriginLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition( (position) => {
        this.requestForm.controls.lat1.patchValue(position.coords.latitude.toFixed(10));
        this.requestForm.controls.lng1.patchValue(position.coords.longitude.toFixed(11));
        this.origin = 'Localização atual';
        document.getElementById('originGps').classList.replace('d-block', 'd-none');
      });
    } else {
      alert('Geolocation não suportada por este navegador.');
    }
  }

  // GET MY DESTINY LOCATION
  getDestinyLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.requestForm.controls.lat2.patchValue(position.coords.latitude.toFixed(10));
        this.requestForm.controls.lng2.patchValue(position.coords.longitude.toFixed(11));
        this.destiny = 'Localização atual';
        document.getElementById('destinyGps').classList.replace('d-block', 'd-none');
      });
    } else {
      alert('Geolocation não suportada por este navegador.');
    }
  }

  getLocationStorage() {
    const origin = document.getElementById('origin') as HTMLInputElement;
    const destiny = document.getElementById('destiny') as HTMLInputElement;
    this.requestForm.patchValue(JSON.parse(sessionStorage.getItem('location')));

    const localization = {
      lat1: this.requestForm.get('lat1').value,
      lng1: this.requestForm.get('lng1').value,
      lat2: this.requestForm.get('lat2').value,
      lng2: this.requestForm.get('lng2').value,
    };
    this.utilsService.getLocalizatonByLatAndLng(localization).subscribe(response => {
      origin.value = response.from;
      destiny.value = response.to;
    });
  }

  saveOnStorage() {
    sessionStorage.setItem('location', JSON.stringify({
      lat1: this.requestForm.get('lat1').value,
      lng1: this.requestForm.get('lng1').value,
      lat2: this.requestForm.get('lat2').value,
      lng2: this.requestForm.get('lng2').value,
      addressOrigin: this.fullAddressOrigin,
      addressDestiny: this.fullAddressDestiny,
      expiration_at: moment().add(5, 'minutes').valueOf()
    }));
  }

  onSubmit() {
    if (this.requestForm.invalid) { return Swal.fire('', 'Preencha todos os campos corretamente antes de continuar', 'warning'); }

    // Swal.fire({
    //   title: 'Confirme a localização',
    //   icon: 'question',
    //   showCancelButton: true,
    //   confirmButtonColor: '#3085d6',
    //   cancelButtonColor: '#d33',
    //   cancelButtonText: 'Cancelar',
    //   confirmButtonText: 'Confirmar',
    //   heightAuto: true,
    //   customClass: {

    //   },
    //   // text: `Partida: ${this.addressOrigin}`,
    //   // tslint:disable-next-line: max-line-length
    // tslint:disable-next-line: max-line-length
    //   html: '<div class="d-flex flex-column p-3">Partida: <strong><origin></origin></strong> <br/> Destino: <strong><destiny></destiny></strong><br/><small>Por favor, inserir número no endereço para uma precisão maior em sua localização</small></div>',
    //   onRender: () => {
    //     const content = Swal.getContent();
    //     const origin = content.querySelector('origin');
    //     const destiny = content.querySelector('destiny');
    //     origin.textContent = this.addressOrigin;
    //     destiny.textContent = this.addressDestiny;
    //   }
    // }).then((result) => {
    //   if (result.isConfirmed) {
    //     this.requestTownService.onRequestSubmit(this.requestForm.value);
    //     this.saveOnStorage();
    //   }
    // });

    this.requestTownService.onRequestSubmit(this.requestForm.value);
    this.saveOnStorage();
  }

  onContactSubmit() {
    if (this.contactForm.invalid) { return Swal.fire('', 'Preencha todos os campos corretamente antes de continuar', 'warning'); }

    this.commonService.sendMail(this.contactForm.value).subscribe(
      response => {
        Swal.fire('', 'Email enviado com sucesso.', 'success').then(
          response => $('#contactUsModal').modal('hide')
        );
      },
      error => {
        Swal.fire('Oops...', 'Houve um erro ao enviar seu email. Tente novamente mais tarde !.', 'error').then(
          response => $('#contactUsModal').modal('hide')
        );
      }
    );
  }
}
