import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { UtilitiesModule } from '../../utilities/utilities.module';
import {MatAutocompleteModule} from '@angular/material/autocomplete';


import { SharedModule } from '../../shared/shared.module';
import { RequestTowComponent } from './pages/request-tow/request-tow.component';
import { ChooseACarComponent } from './pages/choose-a-car/choose-a-car.component';
import { SignupComponent } from './pages/signup/signup.component';
import { AdditionalInfoComponent } from './pages/additional-info/additional-info.component';
import { SigninComponent } from './pages/signin/signin.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { TrackTownComponent } from './pages/track-town/track-town.component';

import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { AuthModule } from '../../core/auth/auth.module';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { OrderSummaryComponent } from './pages/order-summary/order-summary.component';



@NgModule({
  declarations: [
    RequestTowComponent,
    ChooseACarComponent,
    SignupComponent,
    AdditionalInfoComponent,
    SigninComponent,
    PaymentComponent,
    TrackTownComponent,
    ContactUsComponent,
    OrderSummaryComponent
  ],
  imports: [
    CommonModule,
    AuthModule,
    HomeRoutingModule,
    UtilitiesModule,
    SharedModule,
    MatAutocompleteModule,
    GooglePlaceModule
  ]
})
export class HomeModule { }
