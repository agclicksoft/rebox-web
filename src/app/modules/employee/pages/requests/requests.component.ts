import { Component, OnInit } from '@angular/core';
import { CompaniesService } from '../../../../core/services/companies.service';

import * as Ws from '@adonisjs/websocket-client';
import { environment } from '../../../../../environments/environment';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import { Request } from '../../../../shared/classes/request';
import * as moment from 'moment';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.css']
})


export class RequestsComponent implements OnInit {

  socket1;
  socket2;
  channel1;
  channel2;

  id: any;
  idRebox: any;
  isLoading = true;
  requests: Request = new Request();
  page = 1;
  requestPendents: Array<number> = [];

  constructor(
    private companiesService: CompaniesService,
    private spinner: NgxSpinnerService
  ) {
    this.id = localStorage.getItem('c_id');
  }

  ngOnInit(): void {
    this.getRequests();
    this.subscribeInChannel();
  }


  getRequests() {
    this.spinner.show();

    this.companiesService.getRequests(this.id, this.page).subscribe(
      response => {
        this.requests = response;
        this.spinner.hide();
        this.isLoading = false;

        this.requestPendents = [];
        this.checkRefused(this.requests);
        for (const item of this.requests.data) {
          if (item.status === 'pendente') {
            this.requestPendents.push(item.id);
          }
        }
      },
      error => {
        Swal.fire('Oops...', 'Ocorreu um erro ao buscar suas solicitações, tente novamente mais tarde.', 'error');
        this.spinner.hide();
        this.isLoading = false;
      });
  }

  checkRefused(requests) {
    for (const request of requests.data) {
      if (request.reject_partner.length > 0) {
        request.status = 'recusado';
      }
    }
  }

  subscribeInChannel() {
    this.socket1 = Ws(environment.SOCKET_ENDPOINT, { debug: true });

    this.socket1.connect();

    // conectando-se ao canal
    this.channel1 = this.socket1.subscribe('notifications');

    // escutando as atualizações do canal quando o método 'call' foi invocado
    this.channel1.on('new:rebox', event => {
      this.companiesService.getRequests(this.id, this.page).subscribe(
        response => {
          this.requests = response;
          this.isLoading = false;
          this.requestPendents = [];
          for (const item of this.requests.data) {
            if (item.status === 'pendente') { this.requestPendents.push(item.id); }
          }
        },
        error => {
          Swal.fire('Oops...', 'Ocorreu um erro ao buscar suas solicitações, tente novamente mais tarde.', 'error');
          this.isLoading = false;
        }
      );
    });
    // conectando-se ao canal
    this.socket2 = Ws(environment.SOCKET_ENDPOINT, { debug: true });

    this.socket2.connect();
    this.channel2 = this.socket2.subscribe('notifications');

    this.channel2.on('change:rebox', event => {
      this.companiesService.getRequests(this.id, this.page).subscribe(
        response => {
          this.requests = response;
          this.isLoading = false;
          this.requestPendents = [];
          for (const item of this.requests.data) {
            if (item.status === 'pendente') { this.requestPendents.push(item.id); }
          }
        },
        error => {
          Swal.fire('Oops...', 'Ocorreu um erro ao buscar suas solicitações, tente novamente mais tarde.', 'error');
          this.isLoading = false;
        }
      );   });
  }

  changePage(event) {
    this.page = event;
    this.getRequests();
  }
}
