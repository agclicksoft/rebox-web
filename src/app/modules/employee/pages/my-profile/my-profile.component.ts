import { UserService } from 'src/app/core/services/user.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { CpfCnpjValidator } from 'ngx-cpf-cnpj';
import Swal from 'sweetalert2';
import { UtilsService } from 'src/app/core/services/utils.service';
@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css']
})
export class MyProfileComponent implements OnInit {

  employeeId = localStorage.getItem('id');
  imgUrlForm;
  imgPost;
  imgRead;

  employeeForm = this.fb.group({
    first_name: ['', Validators.required],
    last_name: ['', Validators.required],
    name: [''],
    // rg: ['', Validators.required],
    cpf: ['', [Validators.required, CpfCnpjValidator]],
    // cep: [''],
    // address: [''],
    // number: [''],
    // neighborhood: [''],
    // city: [''],
    // uf: [''],
    cel: ['', Validators.required],
    email: ['', Validators.required],
    password: ['', [Validators.required, Validators.minLength(6)]],
    confirm_password: [''],
    vehicles: []
  });

  constructor(
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private userService: UserService,
    private router: Router,
    private utilsService: UtilsService
    ) { }

    ngOnInit(): void {
      this.getEmployee(this.employeeId);
    }

    getEmployee(id): void {
      this.spinner.show();
      this.userService.getUser( id).subscribe(response => {
        this.imgUrlForm = response.img_profile;
        const first_name = response.name.split(' ')[0];
        const last_name = response.name.split(' ')[1];
        this.employeeForm.get('first_name').patchValue(first_name);
        this.employeeForm.get('last_name').patchValue(last_name);
        this.employeeForm.get('confirm_password').patchValue(response.password);
        this.employeeForm.patchValue(response);
        this.spinner.hide();
      }, error => {
        this.spinner.hide();
      });
    }

    sendImg(event): void {
      this.spinner.show();

      this.imgPost = new FormData();
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = () => {
        this.imgUrlForm = (reader.result as string);
      };
      this.imgPost.append('profile', event.target.files[0]);
      this.userService.updateImage(this.imgPost).subscribe(response => {
        this.spinner.hide();
        Swal.fire('', 'Imagem de perfil atualizada com sucesso!', 'success');
        localStorage.removeItem('image_url');
        localStorage.setItem('image_url', response.data.img_profile);
        this.utilsService.headerImg.next(response.data.img_profile);
      });


    }

    onSubmit() {
      const name = `${this.employeeForm.get('first_name').value} ${this.employeeForm.get('last_name').value}`;
      this.employeeForm.get('name').patchValue(name);
      if (this.employeeForm.invalid) {
        return Swal.fire('Campos inválidos', 'Insira os dados corretamente.', 'warning');
      }

      this.spinner.show();
      return this.userService.updateUser(this.employeeId ,this.employeeForm.value).subscribe(response => {
        this.spinner.hide();
        Swal.fire('Atualizado', 'Atendente atualizado com sucesso!', 'success');
        return this.router.navigate(['/employee']);
      }, error => {
        this.spinner.hide();
      });

    }

  }
