import { CompaniesService } from './../../../../core/services/companies.service';
import { Component, OnInit } from '@angular/core';
import { RequestService } from '../../../../core/services/request.service';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import { Request } from '../../../../shared/classes/request';
import { FormBuilder, Validators } from '@angular/forms';
declare var $: any;
@Component({
  selector: 'app-request-detail',
  templateUrl: './request-detail.component.html',
  styleUrls: ['./request-detail.component.css']
})
export class RequestDetailComponent implements OnInit {

  id: number;
  request: Request = new Request();
  firstTime = true;

  confimForm = this.fb.group({
    forecast: ['', Validators.required],
    name_partner: ['', Validators.required],
  });

  constructor(
    private fb: FormBuilder,
    private requestService: RequestService,
    private activatedRoute: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private companiesService: CompaniesService,
    private router: Router
  ) {
    this.id = this.activatedRoute.snapshot.params.id;
  }

  ngOnInit(): void {
    this.getRequest();
  }

  getRequest() {
    if (this.firstTime) {
      this.spinner.show();
    }

    this.requestService.requestById(this.id).subscribe(
      response => {
        this.request = response[0];
        console.log(this.request)
        this.request.distance = parseFloat(this.request.distance).toFixed(2);
        $('#confirm').modal('hide');
        this.spinner.hide();
      },
      error => {
        Swal.fire('Oops...', 'Houve um erro ao buscar sua solicitação', 'error')
        this.spinner.hide();
      }
    )
  }

  acceptRequests() {

    if (this.confimForm.invalid) {
      return Swal.fire('', 'Preencha todos os campos corretamente antes de continuar', 'warning');
    }

    this.spinner.show();

    const companyId = localStorage.getItem('c_id');

    this.requestService.acceptRequest(companyId, this.id, this.confimForm.value).subscribe(
      response => {
        Swal.fire('', 'Solicitação aceita com sucesso.', 'success');
        this.spinner.hide();
        this.getRequest();
      },
      error => {
        Swal.fire('Oops...', 'Ocorreu um erro ao processar sua solicitação, tente novamente mais tarde.', 'error');
        this.spinner.hide();
      }
    );
  }
  rejectRequests() {
    const companyId = localStorage.getItem('c_id');
    Swal.fire({
      title: 'Deseja recusar esta solicitação?',
      icon: 'question',
      confirmButtonText: 'Recusar',
      cancelButtonText: 'Cancelar',
      showCancelButton: true
    }).then( response => {
      if (response.isConfirmed) {
        this.requestService.rejectRequest(companyId, this.id).subscribe(() => {
            Swal.fire('Solicitação recusada', '', 'success').then(() => {
                this.router.navigate(['/employee/requests']);
              }
            );
          }, error => {
            Swal.fire('Oops...', 'Ocorreu um erro ao processar sua solicitação, tente novamente mais tarde.', 'error');
          }
        );
      }
    });


  }

  finishRebox() {
    this.spinner.show();
    this.companiesService.finishRequest(this.id).subscribe(
      response => {
        this.spinner.hide();
        $('#finish').modal('hide');
        Swal.fire('Solicitação finalizada', '', 'success').then(() => this.router.navigate(['/employee/requests'])
        );

      },
      error => {
        Swal.fire('Oops...', 'Ocorreu um erro ao processar sua solicitação, tente novamente mais tarde.', 'error');
        this.spinner.hide();
      }
    );
  }

}
