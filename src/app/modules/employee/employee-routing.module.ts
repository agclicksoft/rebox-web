import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyProfileComponent } from './pages/my-profile/my-profile.component';
import { RequestDetailComponent } from './pages/request-detail/request-detail.component';
import { RequestsComponent } from './pages/requests/requests.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'requests'
      },
      {
        path: 'requests',
        component: RequestsComponent
      },
      {
        path: 'requests/:id',
        component: RequestDetailComponent
      },
      {
        path: 'my-profile',
        component: MyProfileComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
