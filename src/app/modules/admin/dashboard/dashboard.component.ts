import { Component, OnInit } from '@angular/core';
import { AuthService } from './../../../core/auth/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  imgUrl = 'assets/images/user.svg';
  isOpened = false;
  showSide = true;
  constructor(
    private authService: AuthService
  ) { }

  ngOnInit(): void {
  }

  menuToggle() {
    document.getElementById('mySidenav').style.width = '200px';
    this.showSide = false;
  }

  closeNav() {
    document.getElementById('mySidenav').style.width = '0';
    this.showSide = true;
  }

  logout() {
    this.authService.logout('/admin/');
  }

}
