import { LocationService } from './../../../../shared/services/location/location.service';
import { LocationAddress } from './../../../../models/location-address.model';
import { UtilsService } from './../../../../core/services/utils.service';
import { Vehicles } from './../../../../shared/classes/vehicles';
import { NgxSpinnerService } from 'ngx-spinner';
import { AdminService } from './../../../../core/services/admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-customer-edit',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.css']
})
export class CustomerEditComponent implements OnInit {
  customerId;
  vehicles: Vehicles[];
  isEdit = false;

  stateName;
  cityName
  states;
  cities;

  page;

  planId;

  customerForm = this.fb.group({
    name: ['', Validators.required],
    cpf: ['', [Validators.required]],
    email: ['', Validators.required],
    cel: ['', Validators.required],
    city: ['', Validators.required],
    state: ['', Validators.required],
    address_zip_code: [''],
    address_neighborhood: [''],
    address_street: [''],
    plan: [''],
    vehicles: ['']
  });

  vehicleForm = this.fb.group({
    id: [''],
    model: ['', Validators.required],
    brand: ['', Validators.required],
    board: ['', Validators.required],
    motor: [''],
    year: [''],
    oil_changes: ['']
  });

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private adminService: AdminService,
    private spinner: NgxSpinnerService,
    private utilsService: UtilsService,
    private locationService: LocationService,
  ) {
    this.route.params.subscribe(response => {
      this.customerId = response.id;
    })
    this.route.queryParams.subscribe(response => {
      this.page = response.page;
    })
  }

  ngOnInit(): void {
    this.getStates();
  }

  getStates() {
    this.spinner.show();
    this.utilsService.getStates().subscribe(response => {
      this.states = response.map((uf) => {
        const state = {
          id: uf.id,
          sigla: uf.sigla,
          nome: uf.nome
        };
        return state;
      }).sort((a, b) => {
        const x = a.nome.toLowerCase();
        const y = b.nome.toLowerCase();
        if (x < y) {
          return -1;
        }
        if (x > y) {
          return 1;
        }
      });
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    }, () => {
      this.getCustomer(this.customerId);
    });
  }

  onStateChange(event) {
    this.stateName = event;
    this.customerForm.get('state').patchValue(event);
    let idState;
    for (const item of this.states) {
      if (item.sigla === event) {
        idState = item.id;
        this.getCityById(idState);
      }
    }
  }

  getCityById(id) {
    this.utilsService.getCity(id).subscribe(response => {
      this.cities = response;
    });
  }

  onCityChange(event) {
    this.cityName = event;
    this.customerForm.get('city').patchValue(event);
  }

  getCustomer(id) {
    this.spinner.show();
    this.adminService.getCustomerById(id).subscribe(response => {
      if (response.contract.length > 0) {
        this.planId = response.contract[0].plan_id;
      }
      this.customerForm.patchValue(response);
      this.vehicles = response.vehicles;
      this.stateName = response.state;
      this.spinner.hide();
    }, error => {

    }, () => {
      this.onStateChange(this.stateName);
      this.showPlan();
    });
  }

  showPlan() {
    switch (this.planId) {
      case 1:
        this.customerForm.get('plan').patchValue('Rebox Auto');
        break;
      case 2:
        this.customerForm.get('plan').patchValue('Rebox Auto +');
        break
    }
  }

  btnModal() {
    ($('#modalExemplo') as any).modal('show');
    this.vehicleForm.reset();
    this.isEdit = false;
  }

  btnEditModal(vehicle) {
   ($('#modalExemplo') as any).modal('show');
    this.vehicleForm.patchValue(vehicle);
    this.isEdit = true;
  }

  setAddressByCep(cep: string): void {
    const cepSanitized = cep.replace(/\D/g, "");
    if (cepSanitized.length != 8) return
    console.log(cepSanitized)
    this.locationService.fetchAddress(cepSanitized).subscribe(
      (address: LocationAddress) => {
        this.customerForm.get('address_neighborhood').patchValue(address.bairro);
        this.customerForm.get('address_street').patchValue(address.logradouro + ', ');
        this.customerForm.get('state').patchValue(address.uf);
        this.onStateChange(address.uf);
        this.customerForm.get('city').patchValue(address.localidade);


      },
      (error: any) => {
        console.error("setAddressByCep", error);
      }
    );
  }


  updateVehicles() {
    if(this.vehicleForm.valid) {
      if(this.isEdit) {
        for(const vehicle in this.vehicles) {
          if (this.vehicles[vehicle].id === this.vehicleForm.get('id').value) {
            this.vehicles[vehicle] = this.vehicleForm.value;
          }
        }
      } else {
        this.vehicles.push(this.vehicleForm.value);
      }
      ($('#modalExemplo') as any).modal('hide');
    }
  }


  removeCustomer() {
    return Swal.fire({
      title: 'Você tem certeza?',
      text: 'Após confirmar essa ação não poderá ser revertida!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Remover'
    }).then((result) => {
      if (result.isConfirmed) {
        this.adminService.removeCustomer(this.customerId).subscribe(response => {
          Swal.fire(
            'Removido!',
            'Cliente removido com sucesso.',
            'success'
          );
          this.router.navigate(['/admin/customers']);
        });
      }
    });

  }

  onSubmit() {
    this.customerForm.get('vehicles').patchValue(this.vehicles);
    if (this.customerForm.invalid) {
      return Swal.fire('' , 'Preencha todos os campos corretamente antes de continuar', 'warning');
    } else {
      this.spinner.show();
      this.adminService.updateCustomer(this.customerId, this.customerForm.value).subscribe(response => {
        this.spinner.hide();
        Swal.fire({
          icon: 'success',
          title: 'Cliente atualizado com sucesso!'
        });
        return this.router.navigate(['/customers']);
      });
    }
  }

  goBack() {
    this.router.navigate(['/admin/customers'], {
      queryParams: {
        page: this.page
      }
    })
  }

}
