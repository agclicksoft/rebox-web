import { ServicesService } from './../../../../core/services/services.service';
import { FormBuilder, FormControl, FormArray, Form, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilsService } from './../../../../core/services/utils.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from './../../../../core/services/admin.service';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-create-request',
  templateUrl: './create-request.component.html',
  styleUrls: ['./create-request.component.css']
})
export class CreateRequestComponent implements OnInit {

  isTowByPlan = false;
  page;

  formValues = true;
  requestId;
  states;
  cities;

  stateName;
  cityName;

  carPorts = [
    {
      port: 'Carro de Passeio',
      port_car_id: 1
    },
    {
      port: 'Tricíclo',
      port_car_id: 2
    },
    {
      port: 'Quadricíclo',
      port_car_id: 3
    },
    {
      port: 'Van',
      port_car_id: 4
    },
    {
      port: 'Sedan Grande Porte',
      port_car_id: 5
    },
    {
      port: 'Pickup SUV',
      port_car_id: 6
    },
    {
      port: 'Utilitário Carga',
      port_car_id: 7
    },
    {
      port: 'Moto',
      port_car_id: 8
    },
  ];

  location = this.fb.group({
    state: [''],
    city: ['']
  })

  portForm = this.fb.group({
    ports: this.fb.array([])
  });

  planForm = this.fb.group({
    ports: this.fb.array([]),
    services: this.fb.group({
      funeral_assistance: this.fb.group({
        id: [''],
        description: [''],
        cost: [''],
        state: [''],
        city: ['']
      }),
      locksmith: this.fb.group({
        id: [''],
        description: [''],
        cost: [''],
        state: [''],
        city: ['']
      }),
      mechanical_help: this.fb.group({
        id: [''],
        description: [''],
        cost: [''],
        state: [''],
        city: ['']
      }),
      tire_change: this.fb.group({
        id: [''],
        description: [''],
        cost: [''],
        state: [''],
        city: ['']
      }),
      battery_charge: this.fb.group({
        id: [''],
        description: [''],
        cost: [''],
        state: [''],
        city: ['']
      }),
      backup_car: this.fb.group({
        id: [''],
        description: [''],
        cost: [''],
        state: [''],
        city: ['']
      }),
      dry_break: this.fb.group({
        id: [''],
        description: [''],
        cost: [''],
        state: [''],
        city: ['']
      }),
      eletrical_breakdown: this.fb.group({
        id: [''],
        description: [''],
        cost: [''],
        state: [''],
        city: ['']
      }),
      tow: this.fb.group({
        id: [''],
        description: [''],
        cost: [''],
        state: [''],
        city: ['']
      }),
    })
  });

  isArmored = this.fb.group({
    description: ['Veiculo blindado'],
    price: ['', Validators.required]
  });
  upset = this.fb.group({
    description: ['Capotado'],
    price: ['', Validators.required]
  });
  stolenWheel = this.fb.group({
    description: ['Roda furtada'],
    price: ['', Validators.required]
  });
  lockedExchange = this.fb.group({
    description: ['Câmbio travado'],
    price: ['', Validators.required]
  });

  constructor(
    private adminService: AdminService,
    private router: Router,
    private route: ActivatedRoute,
    private utilsService: UtilsService,
    private spinner: NgxSpinnerService,
    private fb: FormBuilder,
    private servicesService: ServicesService
    ) {
      this.route.queryParams.subscribe(response => {
        this.stateName = response.state;
        this.cityName = response.city;
        this.page = response.page;
        this.location.patchValue(response);
      });
    }

    ngOnInit(): void {
      this.getStates();
    }

    getPricesIfEdit() {
      if (this.stateName && this.cityName) {
        this.onStateChange(this.stateName)
        this.getPrices(this.stateName, this.cityName);
      }
    }

    insertForm() {
      for (let i = 0; i < 8; i++) {
        this.ports.push(this.fb.group({
          rebox_commission: [''],
          id: [''],
          prices: this.fb.array([
            this.setPricesControls(), this.setPricesControls(), this.setPricesControls() ])
        }));
      }
      this.plansPort.push(this.fb.group({
        rebox_commission: [''],
        id: [''],
        prices: this.fb.array([
          this.fb.group({
            id: [''],
            price: ['', [Validators.required]],
            state: [''],
            city: ['']
          })
        ])
      }))
    }

    setPricesControls() {
        return this.fb.group({
          id: [''],
          price: ['', [Validators.required]],
          km: [''],
          per_km: [''],
          commission: [''],
          port_car_id: [''],
          state: [''],
          city: ['']
        })
    }

    get ports(): FormArray {
      return this.portForm.get('ports') as FormArray;
    }

    get plansPort(): FormArray {
      return this.planForm.get('ports') as FormArray;
    }


    getPrices(state, city) {
      this.spinner.show();
      this.servicesService.getPrices(state, city, this.isTowByPlan).subscribe(response => {
        this.ports.patchValue(response.ports);
        for (let prices of response.extras) {
          switch(prices.description) {
            case 'Veiculo blindado':
              this.isArmored.patchValue(prices);
              break;
            case 'Capotado':
              this.upset.patchValue(prices);
              break;
            case 'Roda furtada':
              this.stolenWheel.patchValue(prices);
              break;
            case 'Câmbio travado':
              this.lockedExchange.patchValue(prices);
              break;
          }
        }
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
      Swal.fire('', 'Aconteceu um erro inesperado, entre em contato com o administrador do sistema!', 'error')
    }, () => {
      this.getPricesByPlans(this.stateName, this.cityName, true)
    });
  }

  getPricesByPlans(state, city, isPlan) {
    this.spinner.show();
      this.servicesService.getPrices(state, city, isPlan).subscribe(response => {
        this.plansPort.patchValue(response.ports);
        for (let prices of response.extras) {
          switch(prices.description) {
            case 'Assistência funerária':
              this.planForm.get('services').get('funeral_assistance').patchValue(prices);
              break;
            case 'Chaveiro':
              this.planForm.get('services').get('locksmith').patchValue(prices);
              break;
            case 'Socorro mecânico':
              this.planForm.get('services').get('mechanical_help').patchValue(prices);
              break;
            case 'Troca de pneu':
              this.planForm.get('services').get('tire_change').patchValue(prices);;
              break;
            case 'Carga na bateria':
              this.planForm.get('services').get('battery_charge').patchValue(prices);;
              break;
            case 'Carro reserva':
              this.planForm.get('services').get('backup_car').patchValue(prices);;
              break;
            case 'Pane seca':
              this.planForm.get('services').get('dry_break').patchValue(prices);;
              break;
            case 'Pane elétrica':
              this.planForm.get('services').get('eletrical_breakdown').patchValue(prices);;
              break;
            case 'Reboque':
              this.planForm.get('services').get('tow').patchValue(prices);;
              break;
          }
        }
        this.spinner.hide();
      })
  }

  getStates() {
    this.spinner.show();
    this.utilsService.getStates().subscribe(response => {
      this.states = response.map((uf) => {
        const state = {
          id: uf.id,
          sigla: uf.sigla,
          nome: uf.nome
        };
        return state;
      }).sort((a, b) => {
        const x = a.nome.toLowerCase();
        const y = b.nome.toLowerCase();
        if (x < y) {
          return -1;
        }
        if (x > y) {
          return 1;
        }
      });
      this.spinner.hide();
    }, error => {

    }, () => {
      this.insertForm();
      this.getPricesIfEdit();
    });
  }

  onStateChange(event) {
    this.stateName = event;
    let idState;
    for (const item of this.states) {
      if (item.sigla === event) {
        idState = item.id;
        this.getCityById(idState);
      }
    }
  }

  getCityById(id) {
    this.utilsService.getCity(id).subscribe(response => {
      this.cities = response;
    });
  }

  onCityChange(event) {
    this.cityName = event;
    this.getPrices(this.stateName, this.cityName);
  }

  onSubmit() {
    if (!this.isTowByPlan) {
      const prices = {
        ports: this.ports.value,
        extras: [
          this.isArmored.value,
          this.upset.value,
          this.stolenWheel.value,
          this.lockedExchange.value
        ]
      };
      if (this.portForm.valid && this.isArmored.valid && this.upset.valid && this.stolenWheel.valid && this.lockedExchange.valid) {
        this.spinner.show();
        return this.servicesService.updatePrices(prices).subscribe(response => {
          this.spinner.hide();
          Swal.fire('Cadastrado com sucesso!', 'O serviço foi cadastrado com sucesso!', 'success');
        }, error => {
          this.spinner.hide();
          Swal.fire({
            text: `${error.error.message}`,
            icon: 'error'
          })
        });
      }
      return Swal.fire('Por favor.', 'Verifique se os dados foram inseridos corretamente.', 'warning');
    } else {
      const prices = {
        ports: this.planForm.get('ports').value,
        extras: [
          this.planForm.get('services').value.backup_car,
          this.planForm.get('services').value.battery_charge,
          this.planForm.get('services').value.dry_break,
          this.planForm.get('services').value.eletrical_breakdown,
          this.planForm.get('services').value.funeral_assistance,
          this.planForm.get('services').value.locksmith,
          this.planForm.get('services').value.mechanical_help,
          this.planForm.get('services').value.tire_change,
          this.planForm.get('services').value.tow,
        ]
      }
      if (this.planForm.valid) {
        this.spinner.show();
        return this.servicesService.updatePrices(prices).subscribe(response => {
          this.spinner.hide();
          Swal.fire('Cadastrado com sucesso!', 'O serviço foi cadastrado com sucesso!', 'success');
        }, error => {
          this.spinner.hide();
          Swal.fire({
            text: `${error.error.message}`,
            icon: 'error'
          })
        });
      } else {
        console.log(this.planForm.valid)
        const controls = this.planForm.controls;
        for (const name in controls) {
            if (controls[name].invalid) {
                console.log(name);
            }
        }
        Swal.fire('Por favor.', 'Verifique se os dados foram inseridos corretamente.', 'warning');
      }
    }
  }

  removeRequest() {
    return Swal.fire({
      title: 'Você tem certeza?',
      text: 'Após confirmar essa ação não poderá ser revertida!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Remover',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.adminService.removeRequest(this.requestId).subscribe(response => {
          Swal.fire(
            'Removido!',
            'Serviço removido com sucesso.',
            'success'
            );
            this.router.navigate(['/admin/requests']);
          });
        }
      });
    }

    goBack() {
      this.router.navigate(['/admin/services'], {
        queryParams: {
          page: this.page
        }
      })
    }

    setTow(event): void {
      if (event === 'true') {
        this.isTowByPlan = true;
      } else {
        this.isTowByPlan = false;
      }
    }

}
