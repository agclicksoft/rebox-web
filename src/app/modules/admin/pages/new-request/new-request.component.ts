import { UserService } from 'src/app/core/services/user.service';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewRequestService } from './../../../../core/services/new-request.service';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-new-request',
  templateUrl: './new-request.component.html',
  styleUrls: ['./new-request.component.css']
})
export class NewRequestComponent implements OnInit {

  isTow;
  isFuel;
  notFound = false;
  planText;
  planStatus;
  planUtilizations;
  servicesQnt;

  userId;

  userForm = this.fb.group({
    name: ['', Validators.required],
    email: ['', Validators.required],
    cel: [''],
    user_id: ['']
  });

  requests;

  constructor(
    private newRequestService: NewRequestService,
    private userService: UserService,
    private spinner: NgxSpinnerService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
  }

  setUser() {
    if (this.userForm.valid) {
      return this.newRequestService.user.next({
        name: this.userForm.get('name').value,
        email: this.userForm.get('email').value,
        cel: this.userForm.get('cel').value,
        id: this.userId
      });
    }
  }

  checkUserPlan() {
    const email = this.userForm.get('email').value;
    this.spinner.show();
    this.newRequestService.checkUserPlan(email).subscribe((response: any) => {
      this.spinner.hide();
      this.userForm.patchValue(response.user);
      this.userId = response.user.id;
      this.planUtilizations = response.details.uses;
      if (response.details.length !== 0) {
        this.servicesQnt = response.details.use_plans.length;
      }
      this.planText = response.message;
      if (response.message === 'plano ativo') {
        this.planStatus = 'Ativo';
      } else {
        this.planStatus = 'Não Ativo'
      }
      this.notFound = false;
    }, error => {
      this.spinner.hide();
      if (error.status === 404) {
        this.notFound = true;
        this.planStatus = false;
      }
      if(error.status === 401) {
        Swal.fire({
          text: `${error.error.message}`,
          icon: 'warning'
        })
      }
    }, () => {
      this.setUser();
      this.requestHistory(this.userId);
    });
  }

  requestHistory(userId) {
    this.spinner.show();
    this.userService.getHistory(userId).subscribe(response => {
      this.spinner.hide();
      this.requests = response;
    })
  }

  changeService(event) {
    if (parseInt(event.target.value) === 1) {
      this.isTow = true;
      this.isFuel = false;
      // this.setValidatorsForTow();
    }
    else if (parseInt(event.target.value) === 4) {
      this.isFuel = true;
      this.isTow = false;
      // this.removeValidators();
      // this.setValidatorsForFuel();
    }
    else {
      this.isTow = false;
      this.isFuel = false;
      // this.removeValidators();
    }
  }

}
