import { UtilsService } from './../../../../core/services/utils.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { RequestServicesService } from './../../../../core/services/request-services.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-request-service-detail',
  templateUrl: './request-service-detail.component.html',
  styleUrls: ['./request-service-detail.component.css']
})
export class RequestServiceDetailComponent implements OnInit {

  service: any;
  serviceId;
  partners;
  selectedPartnerId;

  page;

  stateName;
  cityName
  states;
  cities;

  constructor(
    private route: ActivatedRoute,
    private requestService: RequestServicesService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private utilsService: UtilsService
  ) {
    this.route.params.subscribe(response => {
      this.serviceId = response.id;
    });
    this.route.queryParams.subscribe(response => {
      this.page = response.page;
    })
  }

  ngOnInit(): void {
    this.getRequestService(this.serviceId);
  }

  getPartnersByStateCity(state, city) {
    this.spinner.show();
    this.requestService.getPartnersByStateCity(state, city).subscribe(response => {
      this.partners = response;
      this.spinner.hide();
    })
  }

  getRequestService(id) {
    let lat;
    let lng;
    this.spinner.show();
    this.requestService.getRequestService(id).subscribe(response => {
      this.service = response;
      lat = response.lat;
      lng = response.lng;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    }, () => {
      if (this.service.status === 'pendente') {
        this.getStates();
      }
    })
  }

  setPartnerId(event) {
    this.selectedPartnerId = event;
  }

  onSubmit() {
    if (!this.selectedPartnerId) {
      return Swal.fire('', 'Para repassar um serviço é necessário selecionar o prestador.', 'warning')
    }
    this.spinner.show();
    const data = {
      partner_id: this.selectedPartnerId
    }
    return this.requestService.transferRequestService(this.serviceId, data).subscribe(response => {
      this.spinner.hide();
      Swal.fire('', 'Solicitação repassada com sucesso!', 'success');
      this.router.navigate(['/admin/requests-services']);
    }, error => {
      Swal.fire({
        text: `${error.error.message}`,
        icon: 'error'
      })
    });
  }

  goBack() {
    this.router.navigate(['/admin/requests-services'], {
      queryParams: {
        page: this.page
      }
    })
  }

  getStates() {
    this.spinner.show();
    this.utilsService.getStates().subscribe(response => {
      this.states = response.map((uf) => {
        const state = {
          id: uf.id,
          sigla: uf.sigla,
          nome: uf.nome
        };
        return state;
      }).sort((a, b) => {
        const x = a.nome.toLowerCase();
        const y = b.nome.toLowerCase();
        if (x < y) {
          return -1;
        }
        if (x > y) {
          return 1;
        }
      });
      this.spinner.hide();
    }, error => {
      this.spinner.hide();

    });
  }

  onStateChange(event) {
    this.stateName = event;
    let idState;
    for (const item of this.states) {
      if (item.sigla === event) {
        idState = item.id;
        this.getCityById(idState);
      }
    }
  }

  getCityById(id) {
    this.utilsService.getCity(id).subscribe(response => {
      this.cities = response;
    });
  }

  onCityChange(event) {
    this.cityName = event;
    this.getPartnersByStateCity(this.stateName, this.cityName);
  }

}
