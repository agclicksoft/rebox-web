import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Customer } from './../../../../shared/classes/customer';
import { NgxSpinnerService } from 'ngx-spinner';
import { AdminService } from './../../../../core/services/admin.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  isLoading = false;
  customers: Customer;
  page = 1;
  keyword = '';

  constructor(
    private adminService: AdminService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.route.queryParams.subscribe(response => {
      if (response.page) {
        this.page = parseInt(response.page);
      }
    });
  }

  ngOnInit(): void {
    this.getCustomers(this.page, this.keyword);
  }

  getCustomers(page, search ) {
    this.spinner.show();
    this.isLoading = true;
    this.adminService.getCustomers(page, search).subscribe(response => {
      this.customers = response;
      this.isLoading = false;
      this.spinner.hide();
    });
  }

  findCustomer(search) {
    this.keyword = search
    if (this.keyword.length >= 3) {
    this.getCustomers(this.page, this.keyword);
    }
    if (!this.keyword) {
      this.getCustomers(1, '');
    }
  }

  changePage(event) {
    this.page = event;
    this.getCustomers(this.page, this.keyword);
  }

  goToCustomer(id) {
    this.router.navigate(['/admin/customers', id], {
      queryParams: {
        page: this.page
      }
    })
  }

}
