import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ReportService } from 'src/app/core/services/report.service';
import { UtilsService } from 'src/app/core/services/utils.service';

@Component({
  selector: 'app-operational-report',
  templateUrl: './operational-report.component.html',
  styleUrls: ['./operational-report.component.css']
})
export class OperationalReportComponent implements OnInit {

  showInput = false;
  uf;
  city;
  period_of;
  period_until;
  bill;
  isAll;
  partnerId;
  cnpj;

  stateName;
  cityName
  states;
  cities;

  operationalForm = this.fb.group({
    partnerId: [''],
    uf: ['', Validators.required],
    city: ['', Validators.required],
    of: ['', Validators.required],
    until: ['', Validators.required],
    bill: ['']
  });

  constructor(
    private fb: FormBuilder,
    private reportService: ReportService,
    private utilsService: UtilsService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.getStates();
  }

  changeCheck() {
    const only = document.getElementById('partner-only') as HTMLInputElement;
    if (only.checked) {
      this.showInput = true;
      this.isAll = false;
    } else {
      this.showInput = false;
      this.isAll = true;
    }
  }

  find() {
    this.period_of = this.operationalForm.get('of').value;
    this.period_until = this.operationalForm.get('until').value;
    this.uf = this.operationalForm.get('uf').value;
    this.city = this.operationalForm.get('city').value;
    this.bill = this.operationalForm.get('bill').value;
    if (this.operationalForm.get('partnerId').value) {
      this.partnerId = `partner_id=${this.operationalForm.get('partnerId').value}`;
    }
  }

  getStates() {
    this.spinner.show();
    this.utilsService.getStates().subscribe(response => {
      this.states = response.map((uf) => {
        const state = {
          id: uf.id,
          sigla: uf.sigla,
          nome: uf.nome
        };
        return state;
      }).sort((a, b) => {
        const x = a.nome.toLowerCase();
        const y = b.nome.toLowerCase();
        if (x < y) {
          return -1;
        }
        if (x > y) {
          return 1;
        }
      });
      this.spinner.hide();
    }, error => {
      this.spinner.hide();

    });
  }

  onStateChange(event) {
    this.stateName = event;
    this.operationalForm.get('uf').patchValue(event);
    let idState;
    for (const item of this.states) {
      if (item.sigla === event) {
        idState = item.id;
        this.getCityById(idState);
      }
    }
  }

  getCityById(id) {
    this.utilsService.getCity(id).subscribe(response => {
      this.cities = response;
    });
  }

  onCityChange(event) {
    this.cityName = event;
    this.operationalForm.get('city').patchValue(event);
  }

}
