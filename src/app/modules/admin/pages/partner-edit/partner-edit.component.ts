import { UtilsService } from './../../../../core/services/utils.service';
import { AdminService } from './../../../../core/services/admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from 'src/app/core/services/user.service';
import Swal from 'sweetalert2';
import { CnpjValidator } from 'src/app/shared/validators/cnpj.validator';
@Component({
  selector: 'app-partner-edit',
  templateUrl: './partner-edit.component.html',
  styleUrls: ['./partner-edit.component.css']
})
export class PartnerEditComponent implements OnInit {

  page;

  partnerId;
  imgPost;
  imgRead;
  imgUrlForm;
  types = [];

  stateName;
  cityName
  states;
  cities;

  isSubmitted = false;

  profileForm = this.fb.group({
    contact1: ['', Validators.required],
    contact2: [''],
    number_trucks: [''],
    cod: [''],
    cnpj: ['', [Validators.required, CnpjValidator]],
    company_name: ['', Validators.required],
    name: [''],
    ie: [''],
    im: [''],
    cep: [''],
    address: [''],
    city: [''],
    uf: [''],
    neighborhood: [''],
    number: [''],
    contract_of: [''],
    contract_until: [''],
    site: [''],
    email: [''],
    types: [''],
    tow: [''],
    changeOil: [''],
    others: [''],
    user: this.fb.group({
      name: [''],
      email: [''],
      password: [''],
    }),
    bank_account: this.fb.group({
      bank_number: [''],
      account_agency: [''],
      account_current : ['']
    })
  });

  userForm = this.fb.group({
    name: [''],
    email: [''],
    password: ['', Validators.minLength(6)],
    confirm_password: [''],
  });

  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private spinner: NgxSpinnerService,
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private utilsService: UtilsService
  ) {
    this.route.params.subscribe(response => {
      // tslint:disable-next-line: radix
      if (response.id !== 'register') {
        this.partnerId = response.id;
      }
    });
    this.route.queryParams.subscribe(response => {
      this.page = response.page;
    });
  }


  ngOnInit(): void {
    this.getStates();
  }

  generateCode() {
    const hexa = '0123456789ABCDEF';
    let code = '';

    for (let i = 0; i < 6; i++) {
      code += hexa[Math.floor(Math.random() * 16)];
    }

    this.profileForm.get('cod').patchValue(code);
  }

  setValidator() {
    this.profileForm.get('user').get('email').setValidators(Validators.required);
    this.profileForm.get('user').get('password').setValidators(Validators.required);
  }

  getStates() {
    this.spinner.show();
    this.utilsService.getStates().subscribe(response => {
      this.states = response.map((uf) => {
        const state = {
          id: uf.id,
          sigla: uf.sigla,
          nome: uf.nome
        };
        return state;
      }).sort((a, b) => {
        const x = a.nome.toLowerCase();
        const y = b.nome.toLowerCase();
        if (x < y) {
          return -1;
        }
        if (x > y) {
          return 1;
        }
      });
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    }, () => {
      if (this.partnerId) {
        this.getPartner(this.partnerId);
      } else {
        // this.setValidator();
        this.generateCode();
      }
    });
  }

  onStateChange(event) {
    this.stateName = event;
    this.profileForm.get('uf').patchValue(event);
    let idState;
    for (const item of this.states) {
      if (item.sigla === event) {
        idState = item.id;
        this.getCityById(idState);
      }
    }
  }

  getCityById(id) {
    this.utilsService.getCity(id).subscribe(response => {
      this.cities = response;
    });
  }

  onCityChange(event) {
    this.cityName = event;
    this.profileForm.get('city').patchValue(event);
  }

  sendImg(event): void {
    this.imgPost = new FormData();

    const reader = new FileReader();
    this.imgRead = true;

    reader.readAsDataURL(event.target.files[0]);
    reader.onload = () => {
      this.imgUrlForm = (reader.result as string);
    };
    this.imgPost.append('profile', event.target.files[0]);
    if (this.partnerId) {
      this.userService.updateImage(this.imgPost).subscribe(response => {
        Swal.fire({
          icon: 'success',
          text: 'Imagem de perfil atualizada com sucesso!'
        });
        localStorage.setItem('image_url', response.data.img_profile);
      }, error => {

      }, () => {

      });
    }
  }

  getPartner(id) {
    this.spinner.show();
    this.adminService.getPartnerById(id).subscribe(response => {
      if (!response.bank_account) {
        response.bank_account = {
          bank_number: '',
          account_agency: '',
          account_current: ''
        };
      }
      this.imgUrlForm = response.user.img_profile;
      this.profileForm.patchValue(response);
      this.profileForm.get('name').patchValue(response.user.name);
      this.userForm.patchValue(response.user);
      this.stateName = response.uf;
      this.spinner.hide();
    }, error => {

    }, () => {
      this.getTypes();
      this.onStateChange(this.stateName);
    });
  }

  getTypes() {
    this.types = this.profileForm.get('types').value.map(type => type.id).sort();
    if (this.types.length > 0) {
      for (let type of this.types) {
        let el = document.getElementById(type) as HTMLInputElement;
        el.checked = true;
      }
    }
  }


  changePassword() {
    if (this.userForm.invalid) {
      return Swal.fire('' , 'Preencha todos os campos corretamente antes de continuar', 'warning');
    }
    this.spinner.show();
    const newPassword = {
      password: this.userForm.get('password').value
    };
    return this.adminService.changePartnerPassword(this.partnerId, newPassword).subscribe(response => {
      this.spinner.hide();
      Swal.fire({
        icon: 'success',
        title: 'Usuário atualizado com sucesso!'
      });
      this.router.navigate(['/admin/partners']);
    });
  }

  removePartner() {
    return Swal.fire({
      title: 'Você tem certeza?',
      text: 'Após confirmar essa ação não poderá ser revertida!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Remover'
    }).then((result) => {
      if (result.isConfirmed) {
        this.adminService.removePartner(this.partnerId).subscribe(response => {
          Swal.fire(
            'Removido!',
            'Prestador removido com sucesso.',
            'success'
          );
          this.router.navigate(['/admin/partners']);
        });
      }
    });
  }

  onSubmit() {
    this.isSubmitted = true;
    this.profileForm.get('types').patchValue(this.types);
    this.userForm.get('name').patchValue(this.profileForm.get('name').value);
    this.profileForm.get('user').patchValue(this.userForm.value);

    if (this.partnerId) {
      if (this.profileForm.invalid) {
        return Swal.fire('' , 'Preencha todos os campos corretamente antes de continuar', 'warning');
      }
      this.spinner.show();
      return this.adminService.updatePartner(this.partnerId, this.profileForm.value).subscribe(reponse => {
        this.spinner.hide();
        Swal.fire({
          icon: 'success',
          title: 'Usuário atualizado com sucesso!'
        });
        this.router.navigate(['/admin/partners']);
      }, error => {
        this.spinner.hide();
        Swal.fire('' , 'Acontenceu um erro inesperado, verifique se os dados estão corretos', 'error');
      });
    }
    this.profileForm.get('email').setValidators(Validators.required);
    this.profileForm.get('email').updateValueAndValidity();
    this.userForm.get('name').patchValue(this.profileForm.get('name').value);
    this.profileForm.get('email').patchValue(this.userForm.get('email').value);
    this.profileForm.get('user').patchValue(this.userForm.value);
    if (this.userForm.invalid || this.profileForm.invalid) {
      return Swal.fire('' , 'Preencha todos os campos corretamente antes de continuar', 'warning');
    }
    this.spinner.show();
    return this.adminService.registerPartner(this.profileForm.value).subscribe(response => {
      this.spinner.hide();
      Swal.fire({
        icon: 'success',
        title: 'Usuário atualizado com sucesso!'
      });
      this.router.navigate(['/admin/partners']);
    }, error => {
      this.spinner.hide();
      Swal.fire('' , 'Acontenceu um erro inesperado, verifique se os dados estão corretos', 'error');
    });

  }

  goBack() {
    this.router.navigate(['/admin/partners'], {
      queryParams: {
        page: this.page
      }
    })
  }

  changeTypeService(event) {
    const index = this.types.indexOf(parseInt(event))
    if (index > -1) {
      this.types.splice(index, 1);
    } else {
      this.types.push(parseInt(event))
    }
  }

}
