import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NewRequestService } from 'src/app/core/services/new-request.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-new-request-service',
  templateUrl: './new-request-service.component.html',
  styleUrls: ['./new-request-service.component.css']
})
export class NewRequestServiceComponent implements OnInit {

  userId;
  planId;

  isTow;
  isFuel;

  options = {
    componentRestrictions: { country: 'BR' }
  };

  services;

  addressOrigin;
  addressDestiny;
  fullAddressOrigin;
  fullAddressDestiny;
  serviceForm = this.fb.group({
    name: [''],
    email: [''],
    address: ['', Validators.required],
    destiny: [''],
    lat: [''],
    lng: [''],
    lat2: [''],
    lng2: ['']
  });

  constructor(
    private fb: FormBuilder,
    private newRequestService: NewRequestService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getUser();
  }

  getUser() {
    this.newRequestService.user.subscribe(response => {
      if (response) {
        this.serviceForm.patchValue(response);
        this.userId = response.id;
      }
    });
    this.getUserServices();
  }

  getUserServices() {
    this.spinner.show();
    this.newRequestService.getUserPlans(this.userId).subscribe(response => {
      this.services = response;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    }, () => {
      this.spinner.hide();
    })
  }

  setOrigin(address: Address) {
    this.addressOrigin = `${address.name} - ${address.formatted_address}`;
    this.fullAddressOrigin = `${address.name} - ${address.formatted_address}`;
    this.serviceForm.get('address').patchValue(this.fullAddressOrigin);
    this.serviceForm.get('lat').patchValue(address.geometry.location.lat());
    this.serviceForm.get('lng').patchValue(address.geometry.location.lng());
    document.getElementById('originGps').classList.replace('d-block', 'd-none');
  }

  originFocus(event) {
    event.length > 0 ? document.getElementById('originGps').classList.replace('d-none', 'd-block')
      : document.getElementById('originGps').classList.replace('d-block', 'd-none');
  }

  setDestiny(address: any) {
    this.addressDestiny = `${address.name} - ${address.formatted_address}`;
    this.fullAddressDestiny = `${address.name} - ${address.formatted_address}`;
    this.serviceForm.get('destiny').patchValue(this.fullAddressDestiny);
    document.getElementById('destinyGps').classList.replace('d-block', 'd-none');
    this.serviceForm.controls.lat2.patchValue(address.geometry.location.lat().toFixed(10));
    this.serviceForm.controls.lng2.patchValue(address.geometry.location.lng().toFixed(11));
  }

  destinyFocus(event) {
    event.length > 0 ? document.getElementById('destinyGps').classList.replace('d-none', 'd-block')
    : document.getElementById('destinyGps').classList.replace('d-block', 'd-none');
  }

  changeService(event) {
    if (parseInt(event.target.value) === 4) {
      this.isFuel = true;
      this.serviceForm.get('destiny').setValidators([Validators.required]);
      this.serviceForm.get('destiny').updateValueAndValidity();
    }
    else {
      this.isFuel = false;
      this.serviceForm.get('destiny').clearValidators();
      this.serviceForm.get('destiny').updateValueAndValidity();
    }
    this.planId = event.target.value;
  }

  onSubmit() {
    if (this.serviceForm.invalid) {
      return Swal.fire('', 'Insira os dados corretamente.', 'warning');
    }
    this.spinner.show();
    return this.newRequestService.consumedUserPlan(this.userId, this.planId, this.serviceForm.value).subscribe(response => {
      this.spinner.hide();
      Swal.fire('', 'Serviço cadastrado com sucesso!', 'success');
      this.router.navigate(['/admin/requests-services']);
    }, error => {
      this.spinner.hide();
    }, () => {
      this.spinner.hide();
    })
  }
}
