import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AuthService } from './../../../../core/auth/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SignInComponent implements OnInit {

  showForgotPassword = false;
  role = 'user';

  loginForm = this.fb.group({
    email: ['', Validators.required],
    password: ['', Validators.required],
    remember_password: [false]
  });

  constructor(
    private fb: FormBuilder,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    if (this.loginForm.invalid) {
      return Swal.fire('', 'Verifique se os dados foram inseridos corretamente.', 'warning');
    }
    return this.authService.loginAdmin(this.loginForm.value);
  }

}
