import { OrderSummaryComponent } from './pages/order-summary/order-summary.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { RequestTowComponent } from './pages/request-tow/request-tow.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { SharedModule } from './../../shared/shared.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SignInComponent } from './pages/signin/signin.component';
import { CustomersComponent } from './pages/customers/customers.component';
import { PartnersComponent } from './pages/partners/partners.component';
import { CustomerEditComponent } from './pages/customer-edit/customer-edit.component';
import { RequestsComponent } from './pages/requests/requests.component';
import { ReportsComponent } from './pages/reports/reports.component';
import { PartnerEditComponent } from './pages/partner-edit/partner-edit.component';
import { CreateRequestComponent } from './pages/create-request/create-request.component';
import { FinancialReportComponent } from './pages/financial-report/financial-report.component';
import { OperationalReportComponent } from './pages/operational-report/operational-report.component';
import { UtilitiesModule } from 'src/app/utilities/utilities.module';
import { RequestsServicesComponent } from './pages/requests-services/requests-services.component';
import { RequestServiceDetailComponent } from './pages/request-service-detail/request-service-detail.component';
import { NewRequestComponent } from './pages/new-request/new-request.component';
import { NewRequestServiceComponent } from './pages/new-request-service/new-request-service.component';

@NgModule({
  declarations: [
    DashboardComponent,
    SignInComponent,
    CustomersComponent,
    PartnersComponent,
    CustomerEditComponent,
    RequestsComponent,
    ReportsComponent,
    PartnerEditComponent,
    CreateRequestComponent,
    FinancialReportComponent,
    OperationalReportComponent,
    RequestTowComponent,
    OrderSummaryComponent,
    RequestsServicesComponent,
    RequestServiceDetailComponent,
    NewRequestComponent,
    NewRequestServiceComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    GooglePlaceModule,
    MatAutocompleteModule,
    UtilitiesModule
  ]
})
export class AdminModule { }
