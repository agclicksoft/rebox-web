import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { PlansService } from './../../../../core/services/plans.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-request-service',
  templateUrl: './request-service.component.html',
  styleUrls: ['./request-service.component.css']
})
export class RequestServiceComponent implements OnInit {

  userId = localStorage.getItem('id') || sessionStorage.getItem('id');
  services;
  hasLimit = false;

  isTow = false;

  selectedPlanDetailId;
  nameService;
  serviceId;

  options = {
    componentRestrictions: { country: 'BR' }
  };

  origin;
  destiny;
  addressOrigin;
  addressDestiny;
  fullAddressOrigin;
  fullAddressDestiny;

  localizationForm = this.fb.group({
    address: ['', Validators.required],
    destiny: [''],
    lat1: [''],
    lng1: [''],
    lat2: [''],
    lng2: ['']
  })

  constructor(
    private plansService: PlansService,
    private spinner: NgxSpinnerService,
    private fb: FormBuilder,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getAvailableServices(this.userId);
  }

  setOrigin(address: Address) {
    this.addressOrigin = `${address.formatted_address}`;
    this.fullAddressOrigin = `${address.formatted_address}`;
    this.localizationForm.get('address').patchValue(this.fullAddressOrigin);
    this.localizationForm.get('lat1').patchValue(address.geometry.location.lat().toFixed(10));
    this.localizationForm.get('lng1').patchValue(address.geometry.location.lng().toFixed(11));
    document.getElementById('originGps').classList.replace('d-block', 'd-none');
  }

  originFocus(event) {
    event.length > 0 ? document.getElementById('originGps').classList.replace('d-none', 'd-block')
      : document.getElementById('originGps').classList.replace('d-block', 'd-none');
  }

  setDestiny(address: any) {
    this.addressDestiny = `${address.name} - ${address.formatted_address}`;
    this.fullAddressDestiny = `${address.name} - ${address.formatted_address}`;
    this.localizationForm.get('destiny').patchValue(this.fullAddressDestiny);
    document.getElementById('destinyGps').classList.replace('d-block', 'd-none');
    this.localizationForm.controls.lat2.patchValue(address.geometry.location.lat().toFixed(10));
    this.localizationForm.controls.lng2.patchValue(address.geometry.location.lng().toFixed(11));
  }

  destinyFocus(event) {
    event.length > 0 ? document.getElementById('destinyGps').classList.replace('d-none', 'd-block')
    : document.getElementById('destinyGps').classList.replace('d-block', 'd-none');
  }

  getAvailableServices(id) {
    this.spinner.show();
    this.plansService.getAvailableServicesOfPlans(id).subscribe(response => {
      this.services = response;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
      Swal.fire({
        text: `${error.error.message}`,
        icon: 'warning'
      }).then((result) => {
        if(result.isConfirmed) {
          return this.router.navigate(['/user/my-plan']);
        }
      })
    });
  }

  onChangeService(event) {
    this.selectedPlanDetailId = parseInt(event);
    if (parseInt(event) === 1 || parseInt(event) === 8) {
      this.isTow = true;
    } else {
      this.isTow = false;
    }

    this.setValidators(parseInt(event));
    this.getNameOfService();
  }

  setValidators(id) {
    if (id === 1 || id === 8) {
      this.localizationForm.get('destiny').setValidators([Validators.required]);
      this.localizationForm.get('destiny').updateValueAndValidity();
    } else {
      this.localizationForm.get('destiny').clearValidators();
      this.localizationForm.get('destiny').reset();
      this.localizationForm.get('lat2').reset();
      this.localizationForm.get('lng2').reset();
      this.localizationForm.get('destiny').updateValueAndValidity();
    }
  }

  openModal() {
    if (this.localizationForm.invalid || !this.serviceId) {
      return Swal.fire({
        text: 'Verifique se os dados foram inseridos corretamente.',
        icon: 'warning'
      })
    }
    this.getNameOfService();
    return ( $('#modalExemplo') as any).modal('show');

  }

  getNameOfService () {
    this.nameService = this.services.filter(service => service.plans_detail_id === this.selectedPlanDetailId)[0].description;
    this.serviceId = this.services.filter(service => service.plans_detail_id === this.selectedPlanDetailId)[0].id;
  }

  consumeService() {
    if (this.localizationForm.invalid || !this.serviceId) {
      return Swal.fire({
        text: 'Verifique se os dados foram inseridos corretamente.',
        icon: 'warning'
      })
    }

    if (this.selectedPlanDetailId === 1 || this.selectedPlanDetailId === 8) {
      return this.router.navigate(['/additional-info'], {
        queryParams: {
          lat1: this.localizationForm.get('lat1').value,
          lng1: this.localizationForm.get('lng1').value,
          lat2: this.localizationForm.get('lat2').value,
          lng2: this.localizationForm.get('lng2').value,
          use_plan: true
        }
      });
    }

    this.spinner.show();
    return this.plansService.consumeService(this.serviceId, this.localizationForm.value).subscribe(response => {
      this.spinner.hide();
      this.router.navigate(['/user/requests-history'])
      Swal.fire({
        text: `${response.message}`,
        icon: 'success'
      })
    }, error => {
      this.spinner.hide();
      Swal.fire({
        text: `${error.error.message}`,
        icon: 'success'
      })
    })
  }

}
