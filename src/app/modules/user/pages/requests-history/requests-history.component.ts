import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-requests-history',
  templateUrl: './requests-history.component.html',
  styleUrls: ['./requests-history.component.css']
})
export class RequestsHistoryComponent implements OnInit {

  userId = localStorage.getItem('id') || sessionStorage.getItem('id');
  requests;
  requestDeitail;

  constructor(
    private userService: UserService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.getHistory(this.userId)
  }

  getHistory(userId) {
    this.spinner.show();
    this.userService.getHistory(userId).subscribe(response => {
      console.log(response);
      this.requests = response;
      this.spinner.hide();
    })
  }

  setRequestToDetail(request) {
    this.requestDeitail = request;
  }

}
