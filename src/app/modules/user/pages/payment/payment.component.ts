import { PlansService } from './../../../../core/services/plans.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import Swal from 'sweetalert2';
import { PaymentService } from '../../../../core/services/payment.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Clipboard } from '@angular/cdk/clipboard';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  plan;
  userId = localStorage.getItem('id');
  payment_method: string;
  validity: string;

  paymentForm = this.fb.group({
    cpf: ['', Validators.required],
    cred_name: ['', Validators.required],
    cred_number: ['', Validators.required],
    cred_due_date_month: [''],
    cred_due_date_year: [''],
    cred_verification: ['', Validators.required],
    portion: [1, Validators.required],
    validity: ['', Validators.required],
    payment_mode: [],
    type_payment_mode: [],
    vehicle_id: []
  });
  reboxService: any;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private clipboard: Clipboard,
    private  plansService: PlansService
  ) {
    this.spinner.show();
    this.route
      .queryParams
      .subscribe(params => {
        this.plan = params.plan;
        this.paymentForm.get('vehicle_id').patchValue(params.vehicle)
        this.spinner.hide();
      });
  }

  ngOnInit(): void {

  }

  copyBarCode(el) {
    this.clipboard.copy(el.value);

    // ($('.btn-cancel') as any).tooltip({title: 'Código copiado', trigger: 'click'});
  }

  onSubmit() {
    this.paymentForm.get('payment_mode').patchValue(this.payment_method);
    if (this.paymentForm.invalid) {
      return Swal.fire('', 'Preencha todos os campos corretamente antes de continuar', 'warning');
    }

    if(this.paymentForm.get('validity').value) {
      this.paymentForm.controls.cred_due_date_month.patchValue(this.paymentForm.value.validity.substr(0, 2));
      this.paymentForm.controls.cred_due_date_year.patchValue(this.paymentForm.value.validity.substr(2));
    }
    this.spinner.show();
    return this.plansService.makePayment(this.userId, this.plan, this.paymentForm.value).subscribe(response => {
      this.spinner.hide();
      this.router.navigate(['/user/my-plan']);
      Swal.fire('', 'Pagamento realizado com sucesso, os serviços serão liberados após 72 horas do pagamento!', 'success');
    }, error => {
      this.spinner.hide();
      Swal.fire({
        title: '',
        text: `${error.error.message? error.error.message : 'Houve um erro inesperado, entre em contato com o administrador do sistema'}.`,
        icon: 'error'
      });
    })
  }

}
