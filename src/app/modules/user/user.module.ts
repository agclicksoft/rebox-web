import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { SharedModule } from './../../shared/shared.module';
import { UserRoutingModule } from './user-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdditionalDataComponent } from './pages/additional-data/additional-data.component';
import { PaymentComponent } from './pages/payment/payment.component';
import {ClipboardModule} from '@angular/cdk/clipboard';
import { MyProfileComponent } from './pages/my-profile/my-profile.component';
import { MyPlanComponent } from './pages/my-plan/my-plan.component';
import { RequestServiceComponent } from './pages/request-service/request-service.component';
import { ServiceSchedulingComponent } from './pages/service-scheduling/service-scheduling.component';
import { RequestsHistoryComponent } from './pages/requests-history/requests-history.component';



@NgModule({
  declarations: [DashboardComponent, AdditionalDataComponent, PaymentComponent, MyProfileComponent, MyPlanComponent, RequestServiceComponent, ServiceSchedulingComponent, RequestsHistoryComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule,
    ClipboardModule,
    GooglePlaceModule,
    MatAutocompleteModule
  ]
})
export class UserModule { }
