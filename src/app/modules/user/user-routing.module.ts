import { RequestServiceComponent } from './pages/request-service/request-service.component';
import { MyPlanComponent } from './pages/my-plan/my-plan.component';
import { MyProfileComponent } from './pages/my-profile/my-profile.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { AdditionalDataComponent } from './pages/additional-data/additional-data.component';
import { SignupComponent } from './../../shared/components/signup/signup.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServiceSchedulingComponent } from './pages/service-scheduling/service-scheduling.component';
import { RequestsHistoryComponent } from './pages/requests-history/requests-history.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'additional-data',
        component: AdditionalDataComponent
      },
      {
        path: 'payment',
        component: PaymentComponent
      },
      {
        path: 'my-profile',
        component: MyProfileComponent
      },
      {
        path: 'my-plan',
        component: MyPlanComponent
      },
      {
        path: 'request-service',
        component: RequestServiceComponent
      },
      {
        path: 'requests-history',
        component: RequestsHistoryComponent
      }
      // {
      //   path: 'service-scheduling',
      //   component: ServiceSchedulingComponent
      // }
    ]
  },
  {
    path: 'signup',
    component: SignupComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
