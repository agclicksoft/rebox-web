import { NgxSpinnerService } from 'ngx-spinner';
import { UtilsService } from './../../../../core/services/utils.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-operational-report',
  templateUrl: './operational-report.component.html',
  styleUrls: ['./operational-report.component.css']
})
export class OperationalReportComponent implements OnInit {

  showInput = false;
  period_of;
  period_until;
  uf;
  city;
  bill;
  isAll;
  partnerId = localStorage.getItem('c_id') ;

  stateName;
  cityName
  states;
  cities;

  operationalForm = this.fb.group({
    partner: [''],
    uf: ['', Validators.required],
    city: ['', Validators.required],
    of: ['', Validators.required],
    until: ['', Validators.required],
    bill: ['']
  });

  constructor(
    private fb: FormBuilder,
    private utilsService: UtilsService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.getStates();
  }

  changeCheck() {
    const only = document.getElementById('partner-only') as HTMLInputElement;
    if (only.checked) {
      this.showInput = true;
      this.isAll = false;
    } else {
      this.showInput = false;
      this.isAll = true;
    }
  }

  openModal() {
    if (this.operationalForm.valid) {
      this.find();
      ($('#ExemploModalCentralizado') as any).modal('show');
    } else {
      Swal.fire('Atenção!', 'Insira os dados corretamente.', 'warning');
    }
  }

  find() {
    this.period_of = this.operationalForm.get('of').value;
    this.period_until = this.operationalForm.get('until').value;
    this.uf = this.operationalForm.get('uf').value;
    this.city = this.operationalForm.get('city').value;
    this.bill = this.operationalForm.get('bill').value;
  }

  getStates() {
    this.spinner.show();
    this.utilsService.getStates().subscribe(response => {
      this.states = response.map((uf) => {
        const state = {
          id: uf.id,
          sigla: uf.sigla,
          nome: uf.nome
        };
        return state;
      }).sort((a, b) => {
        const x = a.nome.toLowerCase();
        const y = b.nome.toLowerCase();
        if (x < y) {
          return -1;
        }
        if (x > y) {
          return 1;
        }
      });
      this.spinner.hide();
    }, error => {
      this.spinner.hide();

    });
  }

  onStateChange(event) {
    this.stateName = event;
    this.operationalForm.get('uf').patchValue(event);
    let idState;
    for (const item of this.states) {
      if (item.sigla === event) {
        idState = item.id;
        this.getCityById(idState);
      }
    }
  }

  getCityById(id) {
    this.utilsService.getCity(id).subscribe(response => {
      this.cities = response;
    });
  }

  onCityChange(event) {
    this.cityName = event;
    this.operationalForm.get('city').patchValue(event);
  }

}
