import { NgxSpinnerService } from 'ngx-spinner';
import { EmployeeService } from './../../../../core/services/employee.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  partnerId = localStorage.getItem('c_id');
  employees;
  page = 1;

  constructor(
    private employeeService: EmployeeService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.getAllEmployeesPartner(this.partnerId, this.page);
  }

  getAllEmployeesPartner(id, page) {
    this.spinner.show();
    this.employeeService.getEmployees(id, page).subscribe(response => {
      this.employees = response;
      this.spinner.hide();
    });
  }

  changePage(event) {
    this.page = event;
    this.getAllEmployeesPartner(this.partnerId, this.page);
  }

}
