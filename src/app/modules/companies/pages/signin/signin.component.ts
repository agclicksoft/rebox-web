import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  showForgotPassword: boolean = false;
  showBackButton: boolean = false;

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
  }

  onSubmit() {

  }

  showForgotPasswordComponent() {
    this.showForgotPassword = true;
    this.showBackButton = true;
  }

  showSigninComponent() {
    this.showForgotPassword = false;
    this.showBackButton = false;
  }

}
