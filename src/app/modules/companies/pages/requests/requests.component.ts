import { environment } from 'src/environments/environment';
import { RequestServicesService } from './../../../../core/services/request-services.service';
import { Component, OnInit } from '@angular/core';
import { CompaniesService } from '../../../../core/services/companies.service';

import * as Ws from '@adonisjs/websocket-client';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import { Request } from '../../../../shared/classes/request';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.css']
})


export class RequestsComponent implements OnInit {

  socket1;
  socket2;
  channel1;
  channel2;

  servicesSocket;
  servicesChannel;

  id: any;
  idRebox: any;
  isLoading: boolean = true;
  requests: Request = new Request();
  page: number = 1;
  requestPendents: Array<number> = [];

  services;
  servicesPage: number = 1;
  servicesPendents = [];
  constructor(
    private companiesService: CompaniesService,
    private spinner: NgxSpinnerService,
    private requestService: RequestServicesService
  ) {
    this.id = localStorage.getItem('c_id');
  }

  ngOnInit(): void {
    this.getRequests();
    this.subscribeInChannel();
  }


  getRequests() {
    this.spinner.show();

    this.companiesService.getRequests(this.id, this.page).subscribe(
      response => {
        this.requests = response;
        this.spinner.hide();
        this.isLoading = false;
        this.requestPendents = [];
        this.checkRefused(this.requests);
        for (let item of this.requests.data) {
          if (item.status === 'pendente') { this.requestPendents.push(item.id); }
        }
      },
      error => {
        Swal.fire('Oops...', 'Ocorreu um erro ao buscar suas solicitações, tente novamente mais tarde.', 'error');
        this.spinner.hide();
        this.isLoading = false;
      }, () => {
        // this.requests.data.sort((a, b) => a > b ? 1 : 1 ).sort((item, itemB) => {
        //   if (item.status === 'pendente') {
        //     return -1;
        //   }
        //   return 0;
        // });
      }
    );
  }

  getRequestsServices(id, page) {
    this.spinner.show();
    this.requestService.getAllServices(id, page).subscribe(response => {
      this.services = response;
      for (let item of this.services.data) {
        if (item.status === 'repassado') { this.servicesPendents.push(item.id); }
      }
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    })
  }

  checkRefused(requests) {
    for (const request of requests.data) {
      if (request.reject_partner.length > 0) {
        request.status = 'recusado';
      }
    }
  }

  subscribeInChannel() {
    this.socket1 = Ws(environment.SOCKET_ENDPOINT, { debug: true });

    this.socket1.connect();

    // conectando-se ao canal
    this.channel1 = this.socket1.subscribe('notifications');

    // escutando as atualizações do canal quando o método 'call' foi invocado
    this.channel1.on('new:rebox', event => {
      this.companiesService.getRequests(this.id, this.page).subscribe(
        response => {
          this.requests = response;
          this.isLoading = false;
          this.requestPendents = [];
          for (let item of this.requests.data) {
            if (item.status === 'pendente') { this.requestPendents.push(item.id); }
          }
        },
        error => {
          Swal.fire('Oops...', 'Ocorreu um erro ao buscar suas solicitações, tente novamente mais tarde.', 'error')
          this.isLoading = false;
        }
      );
    });
    // conectando-se ao canal
    this.socket2 = Ws(environment.SOCKET_ENDPOINT, { debug: true });

    this.socket2.connect();
    this.channel2 = this.socket2.subscribe('notifications');

    this.channel2.on('change:rebox', event => {
      this.companiesService.getRequests(this.id, this.page).subscribe(
        response => {
          this.requests = response;
          this.isLoading = false;
          this.requestPendents = [];
          for (let item of this.requests.data) {
            if (item.status === 'pendente') { this.requestPendents.push(item.id); }
          }
        },
        error => {
          Swal.fire('Oops...', 'Ocorreu um erro ao buscar suas solicitações, tente novamente mais tarde.', 'error')
          this.isLoading = false;
        }
      );   });
  }

  subscribeServiceChannel() {
    this.servicesSocket = Ws(environment.SOCKET_ENDPOINT, { debug: true });

    this.servicesSocket.connect();

    this.servicesChannel = this.servicesSocket.subscribe('notifications');

    this.servicesChannel.on('new:service', event => {
      this.requestService.getAllServices(this.id, this.servicesPage).subscribe(response => {
        this.services = response;
        this.isLoading = false;
        for (let item of this.services.data) {
          if (item.status === 'repassado') { this.servicesPendents.push(item.id); }
        }
      }, error => {
        Swal.fire('Oops...', 'Ocorreu um erro ao buscar seus serviços, tente novamente mais tarde.', 'error')

      })
    });
  }

  changeToService() {
    if(!this.services) {
      this.getRequestsServices(this.id, this.servicesPage);
      this.subscribeServiceChannel();
    }
  }


  changePage(event) {
    this.page = event;
    this.getRequests();
  }

  changeServicePage(event) {
    this.page = event;
    this.getRequestsServices(this.id, this.servicesPage);
  }
}
