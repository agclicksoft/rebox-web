import { OperationalReportComponent } from './pages/operational-report/operational-report.component';
import { FinancialReportComponent } from './pages/financial-report/financial-report.component';
import { EmployeesComponent } from './pages/employees/employees.component';
import { RegisterEmployeeComponent } from './pages/register-employee/register-employee.component';
import { PartnerGuard } from './../../core/guards/partner.guard';
import { ReportsComponent } from './pages/reports/reports.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './../../shared/components/signin/signin.component';
import { SignupComponent } from './pages/signup/signup.component';
import { RequestsComponent } from './pages/requests/requests.component';
import { RequestDetailComponent } from './pages/request-detail/request-detail.component';
import { MyProfileComponent } from './pages/my-profile/my-profile.component';
import { ServiceDetailComponent } from './pages/service-detail/service-detail.component';


const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    canActivate: [PartnerGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'requests'
      },
      {
        path: 'requests',
        component: RequestsComponent
      },
      {
        path: 'requests/:id',
        component: RequestDetailComponent
      },
      {
        path: 'requests/services/:id',
        component: ServiceDetailComponent
      },
      {
        path: 'my-profile',
        component: MyProfileComponent
      },
      {
        path: 'reports',
        component: ReportsComponent
      },
      {
        path: 'reports/financial-report',
        component: FinancialReportComponent
      },
      {
        path: 'reports/operational-report',
        component: OperationalReportComponent
      },
      {
        path: 'employees',
        component: EmployeesComponent
      },
      {
        path: 'register-employee',
        component: RegisterEmployeeComponent
      },
      {
        path: 'edit-employee/:id',
        component: RegisterEmployeeComponent
      }
    ]
  },
  {
    path: 'signin',
    component: SigninComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompaniesRoutingModule { }
