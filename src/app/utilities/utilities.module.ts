import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxMaskModule } from 'ngx-mask';
import { NgxPhoneMaskBrModule } from 'ngx-phone-mask-br'
import { NgxPaginationModule } from 'ngx-pagination';
import { NgSelectModule } from '@ng-select/ng-select';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgxSpinnerModule,
    NgxMaskModule.forRoot(),
    NgxPhoneMaskBrModule,
    NgxPaginationModule,
    NgSelectModule
  ],
  exports: [
    NgxSpinnerModule,
    NgxMaskModule,
    NgxPhoneMaskBrModule,
    NgxPaginationModule,
    NgSelectModule
  ]
})
export class UtilitiesModule { }
