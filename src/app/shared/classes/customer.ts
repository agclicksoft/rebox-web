import { User } from './user';
export class Customer {
  total: string;
  perPage: number;
  page: number;
  lastPage: number;
  data: User[];
}
