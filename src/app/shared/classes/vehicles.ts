import { User } from './user';

export class Vehicles {
    id: number
    user_id: number
    brand: string;
    model: string;
    board: string;
    is_armored: boolean
    created_at: Date
    updated_at: Date
    user: User = new User;
}
