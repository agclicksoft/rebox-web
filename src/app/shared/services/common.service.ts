import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  public openMenu = new EventEmitter();

  constructor() { }
}
