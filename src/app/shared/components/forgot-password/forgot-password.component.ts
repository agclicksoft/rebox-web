import { Component, OnInit, Input } from '@angular/core';
import { CommonService } from '../../../core/services/common.service';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  data: string = '';
  password: string;
  confirmPassword: string;

  userEmail: string;
  resetId: number;

  showPassword: boolean = false;

  @Input() role: string;

  constructor(
    private commonService: CommonService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  forgotPassword() {
    this.spinner.show();

    this.commonService.forgotPassword({cnpj_email: this.data}).subscribe(
      async response => {
        this.spinner.hide();

        this.userEmail = response.data.email;

        const {value: code} = await Swal.fire({
          title: 'Digite o código enviado para o seu email',
          input: 'text',
          inputPlaceholder: 'Código de validação',
          preConfirm: (code) => {
            return this.validateCode(code);
          }
        })
      },
      error => {
        Swal.fire('Oops...', `${error.error.message}`, 'error')
        this.spinner.hide();
      }
    )
  }

  validateCode(code) {
    this.spinner.show();

    const data = {code: code, email: this.userEmail}

    this.commonService.validateCode(data).subscribe(
      response => {
        Swal.close();
        this.resetId = response.id;
        this.showPassword = true;
        this.spinner.hide();
      },
      error => {
        Swal.showValidationMessage(`${error.error.message}`)
        this.spinner.hide();
      }
    )

    return false
  }

  //TODO
  resetPassword() {

    if (!this.password || !this.confirmPassword)
      return Swal.fire('', 'Preencha todos os campos corretamente antes de continuar', 'warning');

    if (this.password.length < 6)
      return Swal.fire('', 'A senha precisa ter no mínimo 06 dígitos.', 'warning');

    if (this.password !== this.confirmPassword)
      return Swal.fire('', 'As senhas precisam ser iguais.', 'warning')

    this.spinner.show();
    
    const data = {
      id: this.resetId,
      password: this.password
    }

    this.commonService.resetPassword(data).subscribe(
      response => {
        Swal.fire('', 'Senha atualizada com sucesso', 'success').then(
          response => {
            location.reload();
          }
        )
        this.spinner.hide();
      },
      error => {
        Swal.fire('Oops...', `${error.error.message}`, 'error')
        this.spinner.hide();
      }
    )
  }

}
