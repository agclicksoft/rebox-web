import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from './../../../../environments/environment.prod';
import { Router } from '@angular/router';
import { Component, Input, OnInit } from '@angular/core';
import { ReportService } from 'src/app/core/services/report.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-report-modal',
  templateUrl: './report-modal.component.html',
  styleUrls: ['./report-modal.component.css']
})
export class ReportModalComponent implements OnInit {
  @Input() uf: string;
  @Input() city: string;
  @Input() of: string;
  @Input() until: string;
  @Input() bill: string;
  @Input() isAll;
  @Input() partnerId;
  @Input() cnpj;

  url;
  role = localStorage.getItem('role');
  toEmail = false;

  constructor(
    private reportService: ReportService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) {
    this.url = this.router.url.split('/')[3];
    console.log(this.url)
  }

  ngOnInit(): void {
  }

  getFinances(type, toEmail) {
    this.spinner.show();
    this.reportService.getFinancesReport(type, this.uf, this.city, this.of, this.until, this.bill, this.partnerId).subscribe(response => {
      this.spinner.hide();
      // tslint:disable-next-line: variable-name
      const type_file = type === 'excel' ? 'xlsx' : 'pdf';
      const newBlob = new Blob([response], {type: `application/${type_file}`});

      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(newBlob);
        return;
      }
      const data = window.URL.createObjectURL(newBlob);
      let link = document.createElement('a');
      link.href = data;
      link.download = `finances_report_of=${this.of}_until=${this.until}.${type_file}`;

      link.dispatchEvent(new MouseEvent('click', {
        bubbles:true,
        cancelable: true,
        view: window
      }));
    }, error => {
      this.spinner.hide();
    });
  }

  sendFinancesToEmail(type) {
    this.spinner.show();
    this.reportService.getFinancesReportToEmail(type, this.uf, this.city, this.of, this.until, this.bill).subscribe(response => {
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }

  getOperationals(type) {
    this.spinner.show();
    this.reportService.getOperationals(type, this.uf, this.city, this.of, this.until, this.bill).subscribe(response => {
      this.spinner.hide();
      // tslint:disable-next-line: variable-name
      const type_file = type === 'excel' ? 'xlsx' : 'pdf';
      const newBlob = new Blob([response], {type: `application/${type_file}`});

      if(window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(newBlob);
        return;
      }
      const data = window.URL.createObjectURL(newBlob);
      let link = document.createElement('a');
      link.href = data;
      link.download = `operationals_report_of_${this.of}_until_${this.until}.${type_file}`;

      link.dispatchEvent(new MouseEvent('click', {
        bubbles: true,
        cancelable: true,
        view: window
      }));
    }, errror => {
      this.spinner.hide();
    });
  }

  sendOperationalsToEmail(type) {
    this.spinner.show();
    // tslint:disable-next-line: max-line-length
    this.reportService.getOperationalReportToEmail(type, this.uf, this.city, this.of, this.until, this.bill).subscribe(response => {
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }

  getAdminFinancesForDownload(type) {
    this.spinner.show();
    this.reportService.getAdminFinancesReportToDownload(
      type, this.uf, this.city, this.of, this.until, this.bill, this.partnerId
      ).subscribe((response: Blob) => {
        this.spinner.hide();
        const type_file = type === 'excel' ? 'xlsx' : 'pdf';

        const newBlob = new Blob([response], { type: `application/${type_file}`});
        if (window.name && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(newBlob);
          return;
        }
        const data = window.URL.createObjectURL(newBlob);
        const link = document.createElement('a');
        link.href = data;
        link.download = `finances_report_of_${this.of}_until_${this.until}.${type_file}`;

        link.dispatchEvent(new MouseEvent('click', {
          bubbles: true,
          cancelable: true,
          view: window
        }));
      }, error => {
        this.spinner.hide();
      });
  }

  getAdminOperationalsForDownload(type) {
    this.spinner.show();
    this.reportService.getAdminOperationalsToDownload(
      type, this.uf, this.city, this.of, this.until, this.bill, this.partnerId
      ).subscribe(response => {
        this.spinner.hide();
      const type_file = type === 'excel' ? 'xlsx' : 'pdf';

      const newBlob = new Blob([response], { type: `application/${type_file}`});
      if (window.name && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(newBlob);
        return;
      }
      const data = window.URL.createObjectURL(newBlob);
      const link = document.createElement('a');
      link.href = data;
      link.download = `operationals_report_of_${this.of}_until_${this.until}.${type_file}`;

      link.dispatchEvent(new MouseEvent('click', {
        bubbles: true,
        cancelable: true,
        view: window
      }));
    }, error => {
      this.spinner.hide();
    });
  }

  viewPdf() {
    if (this.role === 'admin') {
      if (this.url === 'financial-report') {
      window.open(`${environment.API}/reports/finances/pdf?uf=${this.uf}&city=${this.city}&initial_date=${this.of}&final_date=${this.until}&bill=${this.bill}&${this.partnerId}`, '_blank');
      }
      else {
        window.open(`${environment.API}/reports/operationals/pdf?uf=${this.uf}&city=${this.city}&initial_date=${this.of}&final_date=${this.until}&bill=${this.bill}&${this.partnerId}`, '_blank');
      }
    } else {
      if (this.url === 'financial-report') {
        window.open(`${environment.API}/partners/reports/finances/pdf?uf=${this.uf}&city=${this.city}&initial_date=${this.of}&final_date=${this.until}&bill=${this.bill}`, '_blank');
        }
        else {
          window.open(`${environment.API}/partners/reports/operationals/pdf?uf=${this.uf}&city=${this.city}&initial_date=${this.of}&final_date=${this.until}&bill=${this.bill}`, '_blank');
        }
    }
  }

  sendFinancesToAdminEmail(type, email) {
    this.spinner.show();
    this.reportService.getAdminFinancesReportToEmail(
      type, this.uf, this.city, this.of, this.until, this.bill, this.partnerId, email
      ).subscribe(response => {
        this.spinner.hide();
      }, error => {
        this.spinner.hide();
      });
  }

  sendOperationalsToAdminEmail(type, email) {
    this.reportService.getAdminOperationalsReportToEmail(
      type, this.uf, this.city, this.of, this.until, this.bill, this.partnerId, email
      ).subscribe(response => {
        this.spinner.hide();
      }, error => {
        this.spinner.hide();
      });
  }

  downloadReport() {
    const pdf = document.getElementById('pdf') as HTMLInputElement;
    const excel = document.getElementById('excel') as HTMLInputElement;
    if (this.role === 'partner') {
      if (this.url === 'financial-report') {
        if (pdf.checked) {
          this.getFinances('pdf', this.toEmail);
        }
        if (excel.checked) {
          this.getFinances('excel', this.toEmail);
        }
      } else {
        if (pdf.checked) {
          this.getOperationals('pdf');
        }
        if (excel.checked) {
          this.getOperationals('excel');
        }
      }
    } else {
      if (this.url === 'financial-report') {
        if (pdf.checked) {
          this.getAdminFinancesForDownload('pdf');
        }
        if(excel.checked) {
          this.getAdminFinancesForDownload('excel');
        }
      } else {
        if (pdf.checked) {
          this.getAdminOperationalsForDownload('pdf');
        }
        if (excel.checked) {
          this.getAdminOperationalsForDownload('excel');
        }
      }
    }
  }

  sendToEmail(){
    const pdf = document.getElementById('pdf') as HTMLInputElement;
    const excel = document.getElementById('excel') as HTMLInputElement;
    this.toEmail = true;
    if (this.role === 'partner') {
      if (this.url === 'financial-report') {
        if (pdf.checked) {
          this.sendFinancesToEmail('pdf');
        }
        if (excel.checked) {
          this.sendFinancesToEmail('excel');
        }
      } else {
        if (pdf.checked) {
          this.sendOperationalsToEmail('pdf');
        }
        if (excel.checked) {
          this.sendOperationalsToEmail('excel');
        }
      }
    } else {
      if (this.url === 'financial-report') {
        ($('#ExemploModalCentralizado') as any).modal('hide');
        Swal.fire({
          title: 'Insira o email para receber o relatório',
          input: 'email',
          inputAttributes: {
            autocapitalize: 'off',
          },
          showCancelButton: true,
          confirmButtonText: 'Enviar',
          cancelButtonText: 'Cancelar',
          showLoaderOnConfirm: true,
          inputValidator: (value) => {
            if (!value) {
              return 'Email inválido.';
            }
          },
          preConfirm: (email) => {
            if (pdf.checked) {
              this.sendFinancesToAdminEmail('pdf', email);
            }
            if (excel.checked) {
              this.sendFinancesToAdminEmail('excel', email);
            }
          },
          allowOutsideClick: () => !Swal.isLoading()
        });
      } else {
        ($('#ExemploModalCentralizado') as any).modal('hide');
        Swal.fire({
          title: 'Insira o email para receber o relatório',
          input: 'email',
          inputAttributes: {
            autocapitalize: 'off',
          },
          showCancelButton: true,
          confirmButtonText: 'Enviar',
          cancelButtonText: 'Cancelar',
          showLoaderOnConfirm: true,
          inputValidator: (value) => {
            if (!value) {
              return 'Email inválido.';
            }
          },
          preConfirm: (email) => {
            if (pdf.checked) {
              this.sendOperationalsToAdminEmail('pdf', email);
            }
            if (excel.checked) {
              this.sendOperationalsToAdminEmail('excel', email);
            }
          },
          allowOutsideClick: () => !Swal.isLoading()
        });
      }
    }

  }

}
