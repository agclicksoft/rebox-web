import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/auth/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.css']
})
export class PlansComponent implements OnInit {

  isLoggedIn = false;

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.checkIfIsLoggendIn();
  }

  checkIfIsLoggendIn() {
    this.isLoggedIn = this.authService.isLoggedIn();
  }

  next(plan) {
    return this.router.navigate(['/user/signup'], {
      queryParams: {
        plan
      }
    });
  }

}
