import { NgxMaskModule } from 'ngx-mask';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideMenuComponent } from './components/side-menu/side-menu.component';
import { HeaderComponent } from './components/header/header.component';
import { UtilitiesModule } from '../utilities/utilities.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FullSpinnerComponent } from './components/full-spinner/full-spinner.component';
import { ImageHeaderComponent } from './components/image-header/image-header.component';
import { PartialSpinnerComponent } from './components/partial-spinner/partial-spinner.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { SigninFormComponent } from './components/signin-form/signin-form.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { TermsAndConditionsComponent } from './components/terms-and-conditions/terms-and-conditions.component';
import { PageBackComponent } from './components/page-back/page-back.component';
import { ReportModalComponent } from './components/report-modal/report-modal.component';
import { SigninComponent } from './components/signin/signin.component';
import { NgxCurrencyModule } from 'ngx-currency';
import { PlansComponent } from './components/plans/plans.component';
import { SignupComponent } from './components/signup/signup.component';
import { NgxCpfCnpjModule } from 'ngx-cpf-cnpj';


@NgModule({
  declarations: [
    HeaderComponent,
    SideMenuComponent,
    FullSpinnerComponent,
    ImageHeaderComponent,
    PartialSpinnerComponent,
    SigninFormComponent,
    ForgotPasswordComponent,
    PrivacyPolicyComponent,
    TermsAndConditionsComponent,
    PageBackComponent,
    ReportModalComponent,
    SigninComponent,
    PlansComponent,
    SignupComponent
  ],
  imports: [
    CommonModule,
    UtilitiesModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgxMaskModule,
    NgxCurrencyModule,
    NgxCpfCnpjModule
  ],
  exports: [
    HeaderComponent,
    SideMenuComponent,
    FullSpinnerComponent,
    ImageHeaderComponent,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    PartialSpinnerComponent,
    ForgotPasswordComponent,
    SigninFormComponent,
    PrivacyPolicyComponent,
    TermsAndConditionsComponent,
    PageBackComponent,
    NgxPaginationModule,
    NgxMaskModule,
    ReportModalComponent,
    SigninComponent,
    NgxCurrencyModule,
    NgxCpfCnpjModule
  ]
})
export class SharedModule { }
