import { AuthService } from './../auth/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PartnerGuard implements CanActivate {

  constructor(
    private router: Router,
    private authService: AuthService
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const role = localStorage.getItem('role');
      const id = localStorage.getItem('id');
      const token = localStorage.getItem('token');
      const isLoggedIn = this.authService.isLoggedIn();

      if (isLoggedIn &&id && token && role === 'partner') {
        return true;
      }
      this.router.navigate(['/companies/signin']);
      return false;
  }

}
