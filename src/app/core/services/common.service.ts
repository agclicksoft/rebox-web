import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(
    private http: HttpClient
  ) { }

  sendMail(data) {
    return this.http.post<any>(`${environment.API}/contact`, data)
  }

  forgotPassword(data) {
    return this.http.post<any>(`${environment.API}/auth/forgotpassword`, data)
  }

  validateCode(data) {
    return this.http.post<any>(`${environment.API}/auth/validatecode`, data)
  }

  resetPassword(data) {
    return this.http.post<any>(`${environment.API}/auth/resetpassword`, data)
  }
}
