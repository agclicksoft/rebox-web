import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  constructor(
    private http: HttpClient
  ) { }


    getAllServices(page, keyword) {
      return this.http.get(`${environment.API}/admins/prices/cities?page=${page}&search=${keyword}`)
    }

    getPrices(state, city, isPlan = false) {
      return this.http.get<any>(`${environment.API}/admins/prices?&state=${state}&city=${city}&is_plan=${isPlan}`);
    }

    updatePrices(data) {
      return this.http.put<any>(`${environment.API}/admins/prices/999`, data);
    }

}
