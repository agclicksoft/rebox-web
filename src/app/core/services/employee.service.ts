import { environment } from './../../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(
    private http: HttpClient
  ) { }

  getEmployees(id, page) {
    return this.http.get<any>(`${environment.API}/partners/${id}/attendants?page=${page}`);
  }

  getEmployeeById(partner_id, id) {
    return this.http.get<any>(`${environment.API}/partners/${partner_id}/attendants/${id}`);
  }

  createEmployee(id, data) {
    return this.http.post<any>(`${environment.API}/partners/${id}/attendants`, data);
  }

  updateEmployee(partner_id, id, data) {
    return this.http.put<any>(`${environment.API}/partners/${partner_id}/attendants/${id}`, data);
  }

  removeEmployee(partner_id, id) {
    return this.http.delete<any>(`${environment.API}/partners/${partner_id}/attendants/${id}`)
  }

  calculateValue(data) {
    return this.http.post<any>(`${environment.API}/rebox`, data);
  }
}
