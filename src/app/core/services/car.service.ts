import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CarService {

  constructor(
    private http: HttpClient
  ) { }

  getModels(brand, model) {
    return this.http.get<any>(`${environment.API}/cars/models?brand=${brand}&model=${model}`);
  }

  getBrands(brand) {
    return this.http.get<any>(`${environment.API}/cars/brands?brand=${brand}`);
  }
}
