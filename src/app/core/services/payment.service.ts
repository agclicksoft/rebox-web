import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  constructor(
    private http: HttpClient
  ) { }

  requestPayment(id, data) {
    return this.http.post<any>(`${environment.API}/rebox/${id}/payments`, data)
  }
}
