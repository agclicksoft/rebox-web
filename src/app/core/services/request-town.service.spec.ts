import { TestBed } from '@angular/core/testing';

import { RequestTownService } from './request-town.service';

describe('RequestTownService', () => {
  let service: RequestTownService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RequestTownService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
