import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable, EventEmitter, Output } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  public headerImg: BehaviorSubject<string> = new BehaviorSubject<string>(localStorage.getItem('image_url'))

  signupEmitter = new EventEmitter<any>();
  @Output() requestTowEmit = new EventEmitter<any>();
  private apiIbgeUrl = 'https://servicodados.ibge.gov.br/api/v1';

  constructor(
    private http: HttpClient
  ) { }

  getLocalizatonByLatAndLng(localization) {
    return this.http.post<any>(`${environment.API}/google`, localization);
  }

  getStates() {
    return this.http.get<any>(`${this.apiIbgeUrl}/localidades/estados`);
  }

  getCity(id) {
    return this.http.get<any>(`${this.apiIbgeUrl}/localidades/estados/${id}/municipios`);
  }
}
