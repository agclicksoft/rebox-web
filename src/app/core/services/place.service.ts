import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PlaceService {

  constructor(
    private http: HttpClient
  ) { }

  API = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?key=AIzaSyAAY_G1_D1yT5J7mzKqT54Jus3PfzjqoI0&input=';

  getOriginInfo(keyword) {
    return this.http.get<any>(`${this.API}${keyword}`);
  }

  getDestinyInfo(keyword) {
    return this.http.get<any>(`${this.API}${keyword}`);
  }
}
