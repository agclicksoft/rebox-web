import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RequestServicesService {

  constructor(
    private http: HttpClient
  ) { }

  getAllRequestsServices() {
    return this.http.get<any>(`${environment.API}/admins/consumed-plans`)
  }

  getRequestService(id) {
    return this.http.get<any>(`${environment.API}/admins/consumed-plans/${id}`)
  }

  getPartners() {
    return this.http.get<any>(`${environment.API}/admins/partners/state/cities`);
  }

  getPartnersByStateCity(state, city) {
    return this.http.get<any>(`${environment.API}/admins/partners/state/${state}/cities/${city}`);
  }

  transferRequestService (id, data) {
    return this.http.put<any>(`${environment.API}/admins/consumed-plans/${id}`, data)
  }

  getAllServices(id, page) {
    return this.http.get<any>(`${environment.API}/partners/${id}/consumed-plans?page=${page}`);
  }
  getServiceParner(partnerId, id) {
    return this.http.get<any>(`${environment.API}/partners/${partnerId}/consumed-plans/${id}`);
  }

  updateService(partnerId, id, data) {
    return this.http.put<any>(`${environment.API}/partners/${partnerId}/consumed-plans/${id}`, data);

  }
}
