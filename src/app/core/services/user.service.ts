import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient
  ) { }

  getUser(id) {
    return this.http.get<any>(`${environment.API}/users/${id}`);
  }

  updateUser(id, data) {
    return this.http.put<any>(`${environment.API}/users/${id}`, data);
  }

  checkIfExists(data) {
    return this.http.post<any>(`${environment.API}/users/email`, data);
  }

  authenticate(user) {
    return this.http.post<any>(`${environment.API}/auth/authenticate`, user);
  }

  getCars(id) {
    return this.http.get<any>(`${environment.API}/users/${id}/vehicles`);
  }

  appendCar(id, data) {
    return this.http.post<any>(`${environment.API}/users/${id}/vehicles`, data);
  }

  updateImage(data) {
    return this.http.post<any>(`${environment.API}/users/profile`, data);
  }

  getHistory(userId) {
    return this.http.get<any>(`${environment.API}/users/${userId}/history-requests`);
  }

}
