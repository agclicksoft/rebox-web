import { UtilsService } from './../services/utils.service';
import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { environment } from '../../../environments/environment';

import { NgxSpinnerService } from 'ngx-spinner';

import * as moment from 'moment';

import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  loginError = new EventEmitter<any>();

  constructor(
    private http: HttpClient,
    private router: Router,
    private spinner: NgxSpinnerService,
    private utilsService: UtilsService
  ) { }


  public login(user) {
    this.spinner.show();
    return this.http.post<any>(`${environment.API}/auth/authenticate`, user).subscribe(
      response => {
        this.setLocal(response);
        this.router.navigate(['/']);
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
        Swal.fire('', `${error.error.message}`, 'error');
        this.loginError.emit(error);
      }
    );
  }

  // public loginUser(user, route = '')  {
  //   this.spinner.show();
  //   return this.http.post<any>(`${environment.API}/auth/authenticate`, user).subscribe(
  //     response => {
  //       this.setLocal(response);
  //       this.spinner.hide();
  //       route ? this.router.navigate([`/user/${route}`]) : this.router.navigate(['/user']);
  //     },
  //     error => {
  //       this.spinner.hide();
  //       Swal.fire('', `${error.error.message}`, 'error');
  //       this.loginError.emit(error);
  //     }
  //   );
  // }

  public loginAdmin(user) {
    this.spinner.show();
    return this.http.post<any>(`${environment.API}/auth/authenticate/admin`, user).subscribe(
      response => {
        if(user.remember_password) {
          this.setLocal(response);
        } else {
          this.setSession(response);
        }
        this.spinner.hide();
        this.router.navigate(['/admin']);
      },
      error => {
        this.spinner.hide();
        Swal.fire('', `E-mail e/ou senha inválidos.`, 'error');
        this.loginError.emit(error);
      }
    );
  }

  public partnerLogin(company) {
    this.spinner.show();

    return this.http.post<any>(`${environment.API}/auth/authenticate/partner`, company).subscribe(
      response => {
        this.setLocal(response);
        this.router.navigate(['/companies/requests']);
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
        Swal.fire('', `${!error.error.message ? 'CNPJ e/ou senha inválidos.' : error.error.message}`, 'error');
      }
    );
  }

  public employeeLogin(employee) {
    this.spinner.show();
    return this.http.post(`${environment.API}/auth/authenticate/attendant`, employee).subscribe(response => {
      this.setLocal(response);
      this.router.navigate(['/employee/']);
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
      Swal.fire('', `${error.error.message}`, 'error');
    });
  }

  public signup(data) {
    return this.http.post<any>(`${environment.API}/auth/register`, data);
  }

  public signupInPlan(data, plan) {
    this.spinner.show();
    return this.http.post<any>(`${environment.API}/auth/register`, data).subscribe((response: any) => {
      this.spinner.hide();
      this.setSession(response);
      Swal.fire({
        text: response.message,
        icon: 'success'
      })
      this.router.navigate(['/user/payment'], {
        queryParams: {
          plan,
          vehicle: response.user.vehicles[0].id
        }
      });
    }, error => {
      this.spinner.hide();
      Swal.fire('', `${error.error.message}`, 'error');
    });
  }

  public logout(path) {
    localStorage.clear();
    sessionStorage.clear();
    this.utilsService.headerImg.next(null);
    this.router.navigate([`${path}/signin`]);
  }

  private setLocal(response) {
    console.log('Token do usuário logado', response.data.token)
    localStorage.setItem('token', response.data.token);
    localStorage.setItem('id', response.user.id);
    localStorage.setItem('expires_at', JSON.stringify( moment().add(7, 'days').valueOf() ));
    localStorage.setItem('role', response.user.role);
    localStorage.setItem('name', response.user.name);
    if (response.user.partner) {
      localStorage.setItem('c_id', response.user.partner.id);
    }
    if(response.user.img_profile) {
      localStorage.setItem('image_url', response.user.img_profile);
      this.utilsService.headerImg.next(localStorage.getItem('image_url'));
    }
    if (response.user.attendant) {
      localStorage.setItem('a_id', response.user.attendant.id);
      localStorage.setItem('c_id', response.user.attendant.partner_id);
    }
  }

  private setSession(response) {
    sessionStorage.setItem('token', response.data.token);
    sessionStorage.setItem('id', response.user.id);
    sessionStorage.setItem('expires_at', JSON.stringify( moment().add(8, 'hours').valueOf() ));
    sessionStorage.setItem('role', response.user.role);
    sessionStorage.setItem('name', response.user.name);
    if (response.user.partner) {
      sessionStorage.setItem('c_id', response.user.partner.id);
    }
    if(response.user.img_profile) {
      sessionStorage.setItem('image_url', response.user.img_profile);
      this.utilsService.headerImg.next(sessionStorage.getItem('image_url'));
    }
    if (response.user.attendant) {
      sessionStorage.setItem('a_id', response.user.attendant.id);
      sessionStorage.setItem('c_id', response.user.attendant.partner_id);
    }
  }

  public isLoggedIn() {
    const expiration = localStorage.getItem('expires_at') || sessionStorage.getItem('expires_at');
    return moment().isBefore(moment(JSON.parse(expiration)));
  }



}
