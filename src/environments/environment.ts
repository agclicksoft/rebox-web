// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  API: 'https://test-rebox-api.herokuapp.com/v1',
  SOCKET_ENDPOINT: 'wss://test-rebox-api.herokuapp.com/',
  // API: 'https://rebox-test-api.herokuapp.com/v1',
  // SOCKET_ENDPOINT: 'wss://rebox-test-api.herokuapp.com/',
  // API: 'https://rebox-api.herokuapp.com/v1',
  // API: 'http://192.168.1.29:3333/v1',
  // SOCKET_ENDPOINT: 'ws://192.168.1.29:3333/',
  googleAPI: 'https://maps.googleapis.com/maps/api/place/autocomplete/output?json&key=AIzaSyAAY_G1_D1yT5J7mzKqT54Jus3PfzjqoI0'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
